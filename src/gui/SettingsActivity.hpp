#ifndef _SETTINGSACTIVITY_HPP
#define _SETTINGSACTIVITY_HPP

#include <QPushButton>
#include <QTableWidget>
#include <QLabel>
#include <QButtonGroup>

#include <QtSerialPort/QSerialPortInfo>

#include "Activity.hpp"
#include "../persistance/Settings.hpp"

namespace gui {

/*!
 * Creates view with game-keys and serial ports which can be changed.
 */
class SettingsActivity : public Activity {
    Q_OBJECT

  private:
    //! true - if serial ports have been changed/chosen, false - in other cases
    bool serialPortsChanged;
    //! Changes view to MenuActivity
    QPushButton menu;
    //! Shows game-play keys
    QTableWidget * keyboardTable;
    //! Shows available serial ports
    QTableWidget * serialPortTable;
    //! Current settings of application
    persistance::Settings * settings;
    //! Shows number of available serial ports
    QLabel serialPortsCount;
    //! Layout of SettingsActivity, contains every shown widget
    QLayout * layout;
    //! Holds available serial ports
    QList<QSerialPortInfo> ports;
    //! Contains radio buttons for choosing correct serial port for player 1
    QButtonGroup * radioButtonsP1;
    //! Contains radio buttons for choosing correct serial port for player 2
    QButtonGroup * radioButtonsP2;

  protected:
    //! Paints the widget
    void paintEvent(QPaintEvent * event);
    //! Creates table with available serial ports
    void createSerialPortTable();
    //! Creates table with game-keys
    void createKeyboardTable();
    //! Changes text of table item on specific row and column
    void changeKeyboardTableItemValue(int row, int column);
    //! Changes serial port names in settings based on user choices
    void setPortNames();

  public:
    /*!
     * Create instance of SettingsActivity with all widgets, except serialPortTable
     * and button groups which are made in enter.
     */
    SettingsActivity(Window * window, persistance::Settings * settings);
    /*!
     * Shows instance of SettingsActivity.
     * Calls method for creating serialPortTable with all its widgets.
     */
    virtual void enter();
    /*!
     * Method which hides instance of SettingsActivity.
     * Destruct serialPortTable and call method for which sends chosen serial ports
     * to settings.
     */
    virtual void exit();

  private slots:
    /*!
     * Uncheck radio button when radio button from another button group
     * on same row has been clicked.
     */
    void updateRadioButtons(int id);
    /*!
     * Makes instance of QDialog with reimplemented keyboardPressed
     * to be able to change game-key based on row and column of pressed
     * keyboard table item element.
     */
    void editKeyboardSettings(int row, int column);
};

}

#endif
