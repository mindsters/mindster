#include <QBoxLayout>
#include <QColor>
#include <QWidget>
#include <QLayout>

#include "Window.hpp"
#include "Activities.hpp"
#include "../input/Controls.hpp"

gui::Window::Window(const QString & initActivity, persistance::Settings * settings, input::Controls * controls) : QMainWindow() {
  setWindowTitle("Mindster");
  //setMinimumSize(200, 200);
  showMaximized();
  setContentsMargins(0, 0, 0, 0);

  QWidget * centralWidget = new QWidget(this);

  QLayout * layout = new QBoxLayout(QBoxLayout::TopToBottom, centralWidget);
  layout->setContentsMargins(0, 0, 0, 0);
  layout->setSpacing(0);

  centralWidget->setLayout(layout);
  setCentralWidget(centralWidget);

  activities["menu"] = new MenuActivity(this);
  activities["red"] = new SimpleActivity(this, QColor(255, 0, 0, 128), "green");
  activities["green"] = new SimpleActivity(this, QColor(0, 255, 0), "red");
  activities["settings"] = new SettingsActivity(this, settings);
  activities["game"] = new GameActivity(this, controls);
  activities["help"] = new HelpActivity(this);

  QObject::connect(activities["menu"], SIGNAL(newGameClicked()), activities["game"], SLOT(startNewGame()));

  for (auto i = activities.begin(); i != activities.end(); ++i)
    layout->addWidget(*i);

  enterActivity(initActivity);
}

gui::Window::~Window() {
  for (auto i = activities.begin(); i != activities.end(); ++i)
    delete i.value();
}

void gui::Window::enterActivity(const QString & name) {
  currentActivity = activities.find(name);
  Activity * activity = currentActivity.value();
  activity->enter();
}

void gui::Window::setKeyEventProcessor(QObject * processor) {
  QObject::connect(this, SIGNAL(keyPressEvent(QKeyEvent *)), processor, SLOT(handleKeyPressEvent(QKeyEvent *)));
  QObject::connect(this, SIGNAL(keyReleaseEvent(QKeyEvent *)), processor, SLOT(handleKeyReleaseEvent(QKeyEvent *)));
  QObject::connect(processor, SIGNAL(escapeKeyPressed()), this, SLOT(toMenu()));
}

void gui::Window::switchActivity(const QString & name) {
  Activity * activity = currentActivity.value();
  activity->exit();
  enterActivity(name);
}

void gui::Window::toMenu() {
  switchActivity("menu");
}
