#ifndef _GAMEACTIVITY_HPP
#define _GAMEACTIVITY_HPP

#include <QRect>

#include "Activity.hpp"
#include "../model/GameState.hpp"
#include "../graphics/GraphicalRepresentation.hpp"
#include "../graphics/Playfield.hpp"

namespace input {
class Controls;
class ThinkGear;
}

namespace gui {

/*!
 * Creates view with a Playfield.
 */
class GameActivity : public Activity {
    Q_OBJECT
  private:
    //! Holds reference to controls used by a game model.
    input::Controls * controls;
    //! Holds model of the game.
    model::GameState * gameState;
    //! Graphical representation of a game model.
    graphics::GraphicalRepresentation * graphicalRepresentation;
    //! The field which is for rendering the scene.
    graphics::Playfield playfield;
    //! Deletes everything connected to present game.
    void deleteGame();
  protected:
    void paintEvent(QPaintEvent * event);
  public slots:
    //! Exits GameActivity
    void finishedGame(QString str);
    //! Makes new GameState and GraphicalRepresentation.
    void startNewGame();
  public:
    GameActivity(Window * window, input::Controls * controls);
    virtual void enter();
    virtual void exit();
  signals:
    //! Fires when GameActivity was hidden.
    void paused();
    //! Fires when GameActivity was shown.
    void started();
};

}

#endif
