#include <QPainter>
#include <QRect>
#include <QSize>

#include "GameActivity.hpp"
#include "Window.hpp"
#include "../input/Controls.hpp"

gui::GameActivity::GameActivity(Window * window, input::Controls * controls) : Activity(window), controls(controls), gameState(NULL), graphicalRepresentation(NULL), playfield(this) {
  controls->setGameActivitySignals(this);
}

void gui::GameActivity::paintEvent(QPaintEvent * event) {
  QPainter painter(this);
  //painter.setRenderHint(QPainter::HighQualityAntialiasing);
  QRect field = playfield.field();
  painter.setWindow(field);
  int x = (width() - field.width()) / 2;
  int y = (height() - field.height()) / 2;
  painter.setViewport(x, y, field.width(), field.height());
  painter.scale(1, -1);
  playfield.drawScene(&painter);
  QPainter hud(this);
  //hud.setRenderHint(QPainter::HighQualityAntialiasing);
  playfield.drawHUD(&hud, rect());
}

void gui::GameActivity::finishedGame(QString str) {
  QMessageBox box(this);
  box.setText(str);
  box.exec();
  window->switchActivity("menu");
}

void gui::GameActivity::enter() {
  Activity::enter();
  QSize geometry = window->size();
  window->setMinimumSize(geometry);

  if (!gameState) {
    graphicalRepresentation = new graphics::GraphicalRepresentation();
    playfield.setGeometry(geometry);
    gameState = new model::GameState(playfield.field(), controls, graphicalRepresentation);
    QObject::connect(gameState, SIGNAL(finished(QString)), this, SLOT(finishedGame(QString)));
    playfield.setModel(gameState);
  }

  gameState->stopDelayTime();
  emit started();

  window->setFocus();
}

void gui::GameActivity::exit() {
  Activity::exit();
  window->setMinimumSize(QSize(0, 0));

  if (gameState->isFinished()) {
    // e.g. save highscore
    deleteGame();
  } else { // game is not finished
    gameState->startDelayTime();
  }

  emit paused();
}

void gui::GameActivity::deleteGame() {
  playfield.clearModel();
  delete gameState;
  gameState = NULL;
  delete graphicalRepresentation;
  graphicalRepresentation = NULL;
}

void gui::GameActivity::startNewGame() {
  if (gameState != NULL) {
    deleteGame();
  }

  window->switchActivity("game");
}