#include <QPainter>
#include <QRect>
#include <QBoxLayout>

#include "SimpleActivity.hpp"
#include "Window.hpp"

gui::SimpleActivity::SimpleActivity(Window * window, const QColor & color, const QString & next) : Activity(window), color(color), next(next), button(next, this), menu("Main Menu", this) {
  QObject::connect(&button, SIGNAL(clicked()), this, SLOT(switchActivity()));
  QObject::connect(&menu, SIGNAL(clicked()), window, SLOT(toMenu()));

  QBoxLayout * layout = new QBoxLayout(QBoxLayout::LeftToRight, this);
  layout->setAlignment(Qt::AlignTop);
  setLayout(layout);

  layout->addWidget(&button);
  layout->addWidget(&menu);
}

void gui::SimpleActivity::paintEvent(QPaintEvent * event) {
  QPainter painter(this);
  int x, y, width, height;

  x = 0;
  y = 0;
  width = size().width();
  height = size().height();

  painter.fillRect(x, y, width, height, color);
}

void gui::SimpleActivity::switchActivity() {
  window->switchActivity(next);
}
