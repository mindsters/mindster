#ifndef _HELPACTIVITY_HPP
#define _HELPACTIVITY_HPP

#include <QPushButton>
#include <QLabel>

#include "Activity.hpp"

namespace gui {

/*!
 * Creates view with labels with help text.
 */
class HelpActivity : public Activity {
    Q_OBJECT
  private:
    //! Changes view to MenuActivity
    QPushButton menu;
    //! Contains help
    QLabel text;
  protected:
    //! Paints the widget
    void paintEvent(QPaintEvent * event);
  public:
    //! Creates instance of HelpActivity with all widgets.
    HelpActivity(Window * window);

};

}

#endif // _HELPACTIVITY_HPP