#include <QLayout>
#include <QStyleOption>
#include <QPainter>
#include <QLabel>

#include "MenuActivity.hpp"
#include "Window.hpp"

gui::MenuActivity::MenuActivity(Window * window) : Activity(window), newGame("New game", this), settings("Settings", this), game("Continue", this), help("Help", this), exit("Exit", this) {
  layout = new QBoxLayout(QBoxLayout::TopToBottom, this);
  setLayout(layout);
  layout->setAlignment(Qt::AlignCenter);

  QLabel * title = new QLabel("MINDSTER", this);
  title->setStyleSheet("background-color: none; font-size: 48pt; font-weight: bold; margin-bottom: 50px;");

  layout->addWidget(title);
  addButton(&newGame);
  addButton(&game);
  addButton(&settings);
  addButton(&help);
  addButton(&exit);

  QObject::connect(&newGame, SIGNAL(clicked()), this, SLOT(toNewGame()));
  QObject::connect(&settings, SIGNAL(clicked()), this, SLOT(toSettings()));
  QObject::connect(&game, SIGNAL(clicked()), this, SLOT(toGame()));
  QObject::connect(&help, SIGNAL(clicked()), this, SLOT(toHelp()));
  QObject::connect(&exit, SIGNAL(clicked()), window, SLOT(close()));
}

void gui::MenuActivity::addButton(QPushButton * button) {
  layout->addWidget(button);
  button->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
  layout->setAlignment(button, Qt::AlignCenter);
}

void gui::MenuActivity::paintEvent(QPaintEvent * event) {
  QStyleOption opt;
  opt.init(this);
  QPainter p(this);
  style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}

void gui::MenuActivity::toSettings() {
  window->switchActivity("settings");
}

void gui::MenuActivity::toGame() {
  window->switchActivity("game");
}

void gui::MenuActivity::toHelp() {
  window->switchActivity("help");
}

void gui::MenuActivity::toNewGame() {
  emit newGameClicked();
}
