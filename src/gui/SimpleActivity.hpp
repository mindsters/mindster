#ifndef _SIMPLEACTIVITY_HPP
#define _SIMPLEACTIVITY_HPP

#include <QPushButton>

#include "Activity.hpp"

namespace gui {

class SimpleActivity : public Activity {
    Q_OBJECT
  private:
    QColor color;
    QString next;
    QPushButton button;
    QPushButton menu;
  private slots:
    void switchActivity();
  protected:
    void paintEvent(QPaintEvent * event);
  public:
    SimpleActivity(Window * window, const QColor & color, const QString & next);
};

}

#endif
