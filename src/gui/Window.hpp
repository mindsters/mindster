#ifndef _WINDOW_HPP
#define _WINDOW_HPP

#include <QMainWindow>
#include <QMap>
#include <QString>
#include <QKeyEvent>

#include "Activity.hpp"
#include "../persistance/Settings.hpp"

namespace input {
class Controls;
}

namespace gui {

/*!
 * The main class of the application.
 */
class Window : public QMainWindow {
    Q_OBJECT
  private:
    //! Holds the GUI activities.
    QMap<QString, Activity *> activities;
    //! Holds the current GUI activity.
    QMap<QString, Activity *>::const_iterator currentActivity;
    //! Helper method for entering the GUI activity.
    void enterActivity(const QString & name);

  signals:
    //! Fires a signal when a key is pressed.
    void keyPressEvent(QKeyEvent * event);
    //! Fires a signal when a key is released.
    void keyReleaseEvent(QKeyEvent * event);

  public:
    //! Creates the main window.
    Window(const QString & initActivity, persistance::Settings * settings, input::Controls * controls);
    //! Sets the processor of the key events.
    void setKeyEventProcessor(QObject * processor);
    //! Switches from one GUI activity to another.
    void switchActivity(const QString & name);
    //! Deletes the main window.
    ~Window();

  public slots:
    //! Switches to the menu activity.
    void toMenu();
};

}

#endif
