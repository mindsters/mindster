#include <QBoxLayout>
#include <QDialog>
#include <QRadioButton>
#include <QMessageBox>
#include <QStyleOption>
#include <QPainter>

#include "SettingsActivity.hpp"
#include "Window.hpp"

#define COLUMN_ID_BASE 100

gui::SettingsActivity::SettingsActivity(Window * window, persistance::Settings * settings) : Activity(window), settings(settings), menu("Main menu", this), serialPortsCount(this) {
  QObject::connect(&menu, SIGNAL(clicked()), window, SLOT(toMenu()));

  layout = new QBoxLayout(QBoxLayout::TopToBottom, this);
  setLayout(layout);
  layout->setAlignment(Qt::AlignCenter);

  createKeyboardTable();

  QString font("font-size: 12pt; font-weight: bold; text-decoration: underline;");

  QLabel * keyboardLabel = new QLabel("Keyboard:");
  keyboardLabel->setStyleSheet(font);

  QLabel * serialPortLabel = new QLabel("Serial ports:");
  serialPortLabel->setStyleSheet(font);

  layout->addWidget(&menu);
  layout->addWidget(keyboardLabel);
  layout->addWidget(keyboardTable);
  layout->addWidget(serialPortLabel);
  layout->addWidget(&serialPortsCount);
  //serialPortTable is added to layout in enter

  menu.setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
  layout->setAlignment(&menu, Qt::AlignCenter);
}

void gui::SettingsActivity::paintEvent(QPaintEvent * event) {
  QStyleOption opt;
  opt.init(this);
  QPainter p(this);
  style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}

void gui::SettingsActivity::enter() {
  Activity::enter();

  serialPortsChanged = false;
  ports = QSerialPortInfo::availablePorts();
  serialPortsCount.setText("Number of available serial ports: " + QString::number(ports.count()));
  createSerialPortTable();
  layout->addWidget(serialPortTable);
}

void gui::SettingsActivity::exit() {
  setPortNames();
  layout->removeWidget(serialPortTable);
  delete serialPortTable;

  Activity::exit();
}

void gui::SettingsActivity::createSerialPortTable() {
  serialPortTable = new QTableWidget(ports.count(), 2, this);

  QStringList headers;
  headers << "Player 1" << "Player 2";
  serialPortTable->setHorizontalHeaderLabels(headers);

  radioButtonsP1 = new QButtonGroup(serialPortTable);
  radioButtonsP2 = new QButtonGroup(serialPortTable);

  QObject::connect(radioButtonsP1, SIGNAL(buttonClicked(int)), this, SLOT(updateRadioButtons(int)));
  QObject::connect(radioButtonsP2, SIGNAL(buttonClicked(int)), this, SLOT(updateRadioButtons(int)));

  headers.clear();
  int i = 0;

  foreach (const QSerialPortInfo & info, ports) {
    headers << info.portName() + ": " + info.description();

    QRadioButton * button1 = new QRadioButton();
    QRadioButton * button2 = new QRadioButton();

    if (!settings->getPlayer1_portName().compare(info.portName())) {
      button1->setChecked(true);
    }

    if (!settings->getPlayer2_portName().compare(info.portName())) {
      button2->setChecked(true);
    }

    radioButtonsP1->addButton(button1, i);
    radioButtonsP2->addButton(button2, i + COLUMN_ID_BASE);

    serialPortTable->setCellWidget(i, 0, button1);
    serialPortTable->setCellWidget(i, 1, button2);
    i++;
  }

  serialPortTable->setVerticalHeaderLabels(headers);

  serialPortTable->setMinimumSize(700, 0);
  serialPortTable->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
}

void gui::SettingsActivity::createKeyboardTable() {
  keyboardTable = new QTableWidget(4, 2, this);
  QObject::connect(keyboardTable, SIGNAL(cellClicked(int, int)), this, SLOT(editKeyboardSettings(int, int)));
  QObject::connect(keyboardTable, SIGNAL(cellActivated(int, int)), this, SLOT(editKeyboardSettings(int, int)));

  QStringList headers;
  headers << "Player 1" << "Player 2";
  keyboardTable->setHorizontalHeaderLabels(headers);

  headers.clear();
  headers << "Move Left" << "Move Right" << "Gun-Barrel Up" << "Gun-Barrel Down";
  keyboardTable->setVerticalHeaderLabels(headers);

  keyboardTable->setItem(0, 0, new QTableWidgetItem(QKeySequence(settings->getPlayer1_moveLeft()).toString()));
  keyboardTable->setItem(1, 0, new QTableWidgetItem(QKeySequence(settings->getPlayer1_moveRight()).toString()));
  keyboardTable->setItem(2, 0, new QTableWidgetItem(QKeySequence(settings->getPlayer1_gunBarrelUp()).toString()));
  keyboardTable->setItem(3, 0, new QTableWidgetItem(QKeySequence(settings->getPlayer1_gunBarrelDown()).toString()));

  keyboardTable->setItem(0, 1, new QTableWidgetItem(QKeySequence(settings->getPlayer2_moveLeft()).toString()));
  keyboardTable->setItem(1, 1, new QTableWidgetItem(QKeySequence(settings->getPlayer2_moveRight()).toString()));
  keyboardTable->setItem(2, 1, new QTableWidgetItem(QKeySequence(settings->getPlayer2_gunBarrelUp()).toString()));
  keyboardTable->setItem(3, 1, new QTableWidgetItem(QKeySequence(settings->getPlayer2_gunBarrelDown()).toString()));

  keyboardTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
  keyboardTable->setMinimumSize(400, 0);
  keyboardTable->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
}

void gui::SettingsActivity::changeKeyboardTableItemValue(int row, int column) {
  QTableWidgetItem * item;
  item = keyboardTable->item(row, column);

  if (column == 0) {
    switch (row) {
      case 0: item->setText(QKeySequence(settings->getPlayer1_moveLeft()).toString());
        break;

      case 1: item->setText(QKeySequence(settings->getPlayer1_moveRight()).toString());
        break;

      case 2: item->setText(QKeySequence(settings->getPlayer1_gunBarrelUp()).toString());
        break;

      case 3: item->setText(QKeySequence(settings->getPlayer1_gunBarrelDown()).toString());
        break;
    }
  } else {
    switch (row) {
      case 0: item->setText(QKeySequence(settings->getPlayer2_moveLeft()).toString());
        break;

      case 1: item->setText(QKeySequence(settings->getPlayer2_moveRight()).toString());
        break;

      case 2: item->setText(QKeySequence(settings->getPlayer2_gunBarrelUp()).toString());
        break;

      case 3: item->setText(QKeySequence(settings->getPlayer2_gunBarrelDown()).toString());
        break;
    }
  }
}

void gui::SettingsActivity::setPortNames() {
  if (!serialPortsChanged) { //if wasnt changed any serial port, dont check for it
    return;
  }

  QString player1;
  QString player2;
  int buttonsChecked = 0;

  if (radioButtonsP1->checkedButton() != 0) {
    player1 = ports.at(radioButtonsP1->checkedId() % COLUMN_ID_BASE).portName();
    buttonsChecked++;
  }

  if (radioButtonsP2->checkedButton() != 0) {
    player2 = ports.at(radioButtonsP2->checkedId() % COLUMN_ID_BASE).portName();
    buttonsChecked++;
  }

  if (buttonsChecked < 2) {
    QMessageBox box(this);
    box.setIcon(QMessageBox::Critical);
    box.setText("Less than two serial ports have been chosen.");
    box.exec();
    return;
  }

  settings->setPortNames(player1, player2);
}

void gui::SettingsActivity::updateRadioButtons(int id) {
  int column = id / COLUMN_ID_BASE;
  int row = id % COLUMN_ID_BASE;
  QButtonGroup * bGroup;

  if (column == 0) {
    bGroup = radioButtonsP2;
  } else {
    bGroup = radioButtonsP1;
  }

  if (bGroup->checkedButton() != 0 && (bGroup->checkedId() % COLUMN_ID_BASE) == row) {
    bGroup->setExclusive(false);
    bGroup->checkedButton()->setChecked(false);
    bGroup->setExclusive(true);
  }

  serialPortsChanged = true;
}

void gui::SettingsActivity::editKeyboardSettings(int row, int column) {
  class MyDialog : public QDialog {
    private:
      int row, column;
      persistance::Settings * settings;
      SettingsActivity * settingsActivity;

    public:
      MyDialog(int row, int column, persistance::Settings * settings, SettingsActivity * settingsActivity) : QDialog(settingsActivity), row(row), column(column),
        settings(settings), settingsActivity(settingsActivity) {
        QLabel * label = new QLabel("Press any key.", this);
      };

    protected:
      void keyPressEvent(QKeyEvent * event) {
        if (column == 0) {
          switch (row) {
            case 0: settings->setPlayer1_moveLeft((Qt::Key)event->key());
              break;

            case 1: settings->setPlayer1_moveRight((Qt::Key)event->key());
              break;

            case 2: settings->setPlayer1_gunBarrelUp((Qt::Key)event->key());
              break;

            case 3: settings->setPlayer1_gunBarrelDown((Qt::Key)event->key());
              break;
          }
        } else {
          switch (row) {
            case 0: settings->setPlayer2_moveLeft((Qt::Key)event->key());
              break;

            case 1: settings->setPlayer2_moveRight((Qt::Key)event->key());
              break;

            case 2: settings->setPlayer2_gunBarrelUp((Qt::Key)event->key());
              break;

            case 3: settings->setPlayer2_gunBarrelDown((Qt::Key)event->key());
              break;
          }
        }

        settingsActivity->changeKeyboardTableItemValue(row, column);
        close();
      }
  } mDialog(row, column, settings, this);

  mDialog.exec();
}