#include <QLayout>
#include <QStyleOption>
#include <QPainter>

#include "HelpActivity.hpp"
#include "Window.hpp"

#define SEPARATOR "\n\n"
#define TITLE "Game for two players using Mindwave Mobile.\n"
#define HELP "For playing you need keyboard and two Mindwave Mobile headsets.\nGame keys can be changed in Settings.\nHeadsets can be chosen in Settings by selecting correct serial ports.\n"
#define GAME "Game is won by destroying tank of your opponent.\nShooting is done by blinking.\nPower of the bullet is based on your attention, which you can see on sides of the screen."

gui::HelpActivity::HelpActivity(Window * window) : Activity(window), menu("Main menu", this), text(this) {
  QObject::connect(&menu, SIGNAL(clicked()), window, SLOT(toMenu()));

  QLayout * layout = new QBoxLayout(QBoxLayout::TopToBottom, this);
  layout->setAlignment(Qt::AlignCenter);
  setLayout(layout);

  QString str(TITLE);
  str.append(SEPARATOR);
  str.append(HELP);
  str.append(SEPARATOR);
  str.append(GAME);

  text.setAlignment(Qt::AlignCenter);
  text.setText(str);

  layout->addWidget(&menu);
  layout->addWidget(&text);

  menu.setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
  layout->setAlignment(&menu, Qt::AlignCenter);
}

void gui::HelpActivity::paintEvent(QPaintEvent * event) {
  QStyleOption opt;
  opt.init(this);
  QPainter p(this);
  style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}