#include "Activity.hpp"
#include "Window.hpp"

gui::Activity::Activity(Window * window) : window(window), QWidget(window) {
  hide();
}

void gui::Activity::enter() {
  show();
}

void gui::Activity::exit() {
  hide();
}
