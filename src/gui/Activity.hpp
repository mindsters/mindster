#ifndef _ACTIVITY_HPP
#define _ACTIVITY_HPP

#include <QWidget>
#include <QPaintEvent>

namespace gui {

class Window;

/*!
 * Abstract class for routing GUI views.
 */
class Activity : public QWidget {
    Q_OBJECT
  protected:
    //! The instance of main application.
    Window * window;
    //! Pure method for custom painting.
    void paintEvent(QPaintEvent * event) = 0;
  public:
    //! Takes the main application instance.
    Activity(Window * window);
    //! Shows the view; it's called during switch into the view.
    virtual void enter();
    //! Hides the view; it's called during switch out of the view.
    virtual void exit();
};

}

#endif
