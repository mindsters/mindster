#ifndef _MENUACTIVITY_HPP
#define _MENUACTIVITY_HPP

#include <QPushButton>

#include "Activity.hpp"

namespace gui {

/*!
 * Creates view with buttons for navigation through the user interface.
 */
class MenuActivity : public Activity {
    Q_OBJECT
  private:
    //! Main layout of this view.
    QLayout  * layout;
    //! Changes view to GameActivity and starts new game if necessary
    QPushButton newGame;
    //! Changes view to SettingsActivity
    QPushButton settings;
    //! Changes view to GameActivity
    QPushButton game;
    //! Changes view to HelpActivity
    QPushButton help;
    //! Exits application
    QPushButton exit;
    //! Adds button to layout
    void addButton(QPushButton * button);
  private slots:
    //! Switches shown activity to SettingsActivity
    void toSettings();
    //! Switches shown activity to GameActivity
    void toGame();
    //! Switches shown activity to HelpActivity
    void toHelp();
    //! Handle clicking on button newGame.
    void toNewGame();
  protected:
    //! Paints the widget
    void paintEvent(QPaintEvent * event);
  public:
    //! Create instance of MenuActivity with all widgets.
    MenuActivity(Window * window);
  signals:
    //! Fires when newGame button is clicked
    void newGameClicked();
};

}

#endif
