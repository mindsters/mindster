#include <QApplication>
#include <QFile>
#include <QTextStream>

#include "gui/Window.hpp"
#include "persistance/Settings.hpp"
#include "input/Controls.hpp"

int main(int argc, char * argv[]) {
  QApplication app(argc, argv);

  persistance::JSONSerializer * serializer = new persistance::JSONSerializer("text.json");
  persistance::Settings * settings = new persistance::Settings(serializer);
  input::Controls * controls = new input::Controls(settings);
  gui::Window window("menu", settings, controls);
  controls->setKeyboardAsProcessor(&window);

  QFile styleFile("./style.css");

  if (styleFile.open(QIODevice::ReadOnly)) {
    QTextStream textStream(&styleFile);
    QString styleSheet = textStream.readAll();
    styleFile.close();
    window.setStyleSheet(styleSheet);
  }

  window.show();

  int retval = app.exec();

  delete controls;
  delete settings;
  delete serializer;

  return retval;
}
