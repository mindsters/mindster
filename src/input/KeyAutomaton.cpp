#include <QDebug>

#include "KeyAutomaton.hpp"

input::KeyAutomaton::KeyAutomaton() : machine(new QStateMachine()), Nothing(new QState()), Positive(new QState()), Negative(new QState()), Positive_after_Negative(new QState()), Negative_after_Positive(new QState()) {
  QSignalTransition * t;

  t = new QSignalTransition(this, SIGNAL(setPositive()), Nothing);
  QObject::connect(t, SIGNAL(triggered()), this, SLOT(slotToPositive()));
  t->setTargetState(Positive);
  Nothing->addTransition(t); // changedToPositive

  t = new QSignalTransition(this, SIGNAL(setNegative()), Nothing);
  QObject::connect(t, SIGNAL(triggered()), this, SLOT(slotToNegative()));
  t->setTargetState(Negative);
  Nothing->addTransition(t); // changedToNegative


  t = new QSignalTransition(this, SIGNAL(clearPositive()), Positive);
  QObject::connect(t, SIGNAL(triggered()), this, SLOT(slotToNothing()));
  t->setTargetState(Nothing);
  Positive->addTransition(t); // changedToNothing

  t = new QSignalTransition(this, SIGNAL(setNegative()), Positive);
  QObject::connect(t, SIGNAL(triggered()), this, SLOT(slotToNegative()));
  t->setTargetState(Negative_after_Positive);
  Positive->addTransition(t); // changedToNegative


  t = new QSignalTransition(this, SIGNAL(clearNegative()), Negative);
  QObject::connect(t, SIGNAL(triggered()), this, SLOT(slotToNothing()));
  t->setTargetState(Nothing);
  Negative->addTransition(t); // changedToNothing

  t = new QSignalTransition(this, SIGNAL(setPositive()), Negative);
  QObject::connect(t, SIGNAL(triggered()), this, SLOT(slotToPositive()));
  t->setTargetState(Positive_after_Negative);
  Negative->addTransition(t); // changedToPositive


  t = new QSignalTransition(this, SIGNAL(clearPositive()), Positive_after_Negative);
  QObject::connect(t, SIGNAL(triggered()), this, SLOT(slotToNegative()));
  t->setTargetState(Negative);
  Positive_after_Negative->addTransition(t); // changedToNegative

  Positive_after_Negative->addTransition(this, SIGNAL(clearNegative()), Positive); // --


  t = new QSignalTransition(this, SIGNAL(clearNegative()), Negative_after_Positive);
  QObject::connect(t, SIGNAL(triggered()), this, SLOT(slotToPositive()));
  t->setTargetState(Positive);
  Negative_after_Positive->addTransition(t); // changedToPositive

  Negative_after_Positive->addTransition(this, SIGNAL(clearPositive()), Negative); // --


  machine->addState(Nothing);
  machine->addState(Positive);
  machine->addState(Negative);
  machine->addState(Positive_after_Negative);
  machine->addState(Negative_after_Positive);

  machine->setInitialState(Nothing);
  machine->start();
}

input::KeyAutomaton::~KeyAutomaton() {
  machine->stop();
  delete Negative_after_Positive;
  delete Positive_after_Negative;
  delete Negative;
  delete Positive;
  delete Nothing;
  delete machine;
}

void input::KeyAutomaton::changePositive(bool pressed) {
  if (pressed) {
    emit setPositive();
  } else {
    emit clearPositive();
  }
}
void input::KeyAutomaton::changeNegative(bool pressed) {
  if (pressed) {
    emit setNegative();
  } else {
    emit clearNegative();
  }
}

void input::KeyAutomaton::slotToPositive() {
  emit changedToPositive();
}
void input::KeyAutomaton::slotToNegative() {
  emit changedToNegative();
}
void input::KeyAutomaton::slotToNothing() {
  emit changedToNothing();
}
