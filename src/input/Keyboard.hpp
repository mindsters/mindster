#ifndef _KEYBOARD_HPP
#define _KEYBOARD_HPP

#include <QKeyEvent>

#include "Controls.hpp"
#include "GameKey.hpp"

namespace input {

/*!
 * Processor of keyboard input events.
 */
class Keyboard : public QObject {
    Q_OBJECT

  private:
    //! Holds settings of keyboard.
    persistance::Settings * settings;
    //! Holds the main instance of model controlling.
    Controls * controls;

  public:
    Keyboard(Controls * controls, persistance::Settings * settings);
  public slots:
    //! Processes key press events.
    void handleKeyPressEvent(QKeyEvent * event);
    //! Processes key release events.
    void handleKeyReleaseEvent(QKeyEvent * event);
  signals:
    //! Fires the states of keys.
    void gameKeyState(GameKey gameKey, bool pressed);
    void escapeKeyPressed();
};

}

#endif // _KEYBOARD_HPP