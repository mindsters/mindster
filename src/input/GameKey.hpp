#ifndef _GAMEKEY_HPP
#define _GAMEKEY_HPP

namespace input {

/*!
 * Keys used by players.
 */
enum GameKey {
  PL1_UP,
  PL1_DOWN,
  PL1_LEFT,
  PL1_RIGHT,
  PL2_UP,
  PL2_DOWN,
  PL2_LEFT,
  PL2_RIGHT
};

}

#endif