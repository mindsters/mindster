#ifndef _DOUBLEACTIONDETECTOR_HPP
#define _DOUBLEACTIONDETECTOR_HPP

#include <QObject>
#include <QElapsedTimer>
#include <QVector>

namespace input {

/*!
 * Adaptive detector of double actions.
 */
class DoubleActionDetector : public QObject {
    Q_OBJECT
  private:
    //! True if there wasn't ation after enabling yet.
    bool firstAction;
    //! Timer for analysation.
    QElapsedTimer timer;
    //! Upper bound of the detection window relative to the average period.
    qreal detectionSpan;
    //! Lower bound of averaging window relative to the average period.
    qreal avgWindow;
    //! Average period between actions.
    qint64 avgPeriod;
  public:
    DoubleActionDetector(qreal dS = defaultDetectionSpan, qreal aW = defaultAvgWindow, qint64 iP = defaultInitialPeriod);
    ~DoubleActionDetector();

    //! Coefficient of the exponential moving average.
    static const qreal steepness;
    //! Upper bound of the detection window relative to the average period.
    static const qreal defaultDetectionSpan;
    //! Lower bound of averaging window relative to the average period.
    static const qreal defaultAvgWindow;
    //! Initial period of actions.
    static const qint64 defaultInitialPeriod;

  public slots:
    //! Incoming triggers.
    void triggerAction();
    //! Invalidate analysing timer.
    void inputDisabled();
    //! Resets analysing timer.
    void inputEnabled();
  signals:
    //! Outcoming triggers of a double action.
    void triggerDoubleAction();
};

}

#endif
