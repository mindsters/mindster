#include "DoubleActionDetector.hpp"
#include <QDebug>

input::DoubleActionDetector::DoubleActionDetector(qreal dS, qreal aW, qint64 iP) : firstAction(true), timer(), detectionSpan(dS), avgWindow(aW), avgPeriod(iP) {
  timer.start();
  //timer.invalidate();
}

input::DoubleActionDetector::~DoubleActionDetector() {
  timer.invalidate();
}

const qreal input::DoubleActionDetector::steepness = 0.3;

const qreal input::DoubleActionDetector::defaultDetectionSpan = 0.3;

const qreal input::DoubleActionDetector::defaultAvgWindow = 0.4;

const qint64 input::DoubleActionDetector::defaultInitialPeriod = 1000;

void input::DoubleActionDetector::triggerAction() {
  if (!timer.isValid()) {
    return;
  }

  if (firstAction) {
    firstAction = false;
    timer.restart();
    return;
  }

  qint64 diff = timer.elapsed();
  //qDebug() << diff << "\t" << avgPeriod;

  if (diff <= defaultDetectionSpan * avgPeriod) {
    emit triggerDoubleAction();
  }

  if (diff >= defaultAvgWindow * avgPeriod) {
    // Exponential moving average
    avgPeriod = (qint64)(steepness * diff + (1 - steepness) * avgPeriod);
  }

  timer.restart();
}

void input::DoubleActionDetector::inputDisabled() {
  timer.invalidate();
  firstAction = true;
}

void input::DoubleActionDetector::inputEnabled() {
  timer.restart();
}
