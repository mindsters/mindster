#include "Keyboard.hpp"
#include <QDebug>

input::Keyboard::Keyboard(Controls * controls, persistance::Settings * settings) : controls(controls), settings(settings) {
  QObject::connect(this, SIGNAL(gameKeyState(GameKey, bool)), controls, SLOT(fromKeyboard(GameKey, bool)));
}

void input::Keyboard::handleKeyPressEvent(QKeyEvent * event) {
  Qt::Key key = (Qt::Key)event->key();

  if (event->isAutoRepeat()) {
    return;
  }

  if (key == settings->getPlayer1_moveLeft()) {
    emit gameKeyState(GameKey::PL1_LEFT, true);
  } else if (key == settings->getPlayer1_moveRight()) {
    emit gameKeyState(GameKey::PL1_RIGHT, true);
  } else if (key == settings->getPlayer1_gunBarrelUp()) {
    emit gameKeyState(GameKey::PL1_UP, true);
  } else if (key == settings->getPlayer1_gunBarrelDown()) {
    emit gameKeyState(GameKey::PL1_DOWN, true);
  } else if (key == settings->getPlayer2_moveLeft()) {
    emit gameKeyState(GameKey::PL2_LEFT, true);
  } else if (key == settings->getPlayer2_moveRight()) {
    emit gameKeyState(GameKey::PL2_RIGHT, true);
  } else if (key == settings->getPlayer2_gunBarrelUp()) {
    emit gameKeyState(GameKey::PL2_UP, true);
  } else if (key == settings->getPlayer2_gunBarrelDown()) {
    emit gameKeyState(GameKey::PL2_DOWN, true);
  } else if (key == Qt::Key_Escape) {
    emit escapeKeyPressed();
  }
}

void input::Keyboard::handleKeyReleaseEvent(QKeyEvent * event) {
  Qt::Key key = (Qt::Key)event->key();

  if (event->isAutoRepeat()) {
    return;
  }

  if (key == settings->getPlayer1_moveLeft()) {
    emit gameKeyState(GameKey::PL1_LEFT, false);
  } else if (key == settings->getPlayer1_moveRight()) {
    emit gameKeyState(GameKey::PL1_RIGHT, false);
  } else if (key == settings->getPlayer1_gunBarrelUp()) {
    emit gameKeyState(GameKey::PL1_UP, false);
  } else if (key == settings->getPlayer1_gunBarrelDown()) {
    emit gameKeyState(GameKey::PL1_DOWN, false);
  } else if (key == settings->getPlayer2_moveLeft()) {
    emit gameKeyState(GameKey::PL2_LEFT, false);
  } else if (key == settings->getPlayer2_moveRight()) {
    emit gameKeyState(GameKey::PL2_RIGHT, false);
  } else if (key == settings->getPlayer2_gunBarrelUp()) {
    emit gameKeyState(GameKey::PL2_UP, false);
  } else if (key == settings->getPlayer2_gunBarrelDown()) {
    emit gameKeyState(GameKey::PL2_DOWN, false);
  }
}
