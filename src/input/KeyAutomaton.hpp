#ifndef _KEYAUTOMATON_HPP
#define _KEYAUTOMATON_HPP

#include <QObject>
#include <QStateMachine>
#include <QState>
#include <QSignalTransition>
#include <QList>

namespace input {

/*!
 * Automaton for resolving signal fires during overlapping keystrokes.
 */
class KeyAutomaton : public QObject {
    Q_OBJECT

  private:
    /*!
     * An FSM
     */
    QStateMachine * machine;
    QState * Nothing;
    QState * Positive;
    QState * Negative;
    QState * Positive_after_Negative;
    QState * Negative_after_Positive;
  public:
    KeyAutomaton();
    ~KeyAutomaton();

  private slots:
    void slotToPositive();
    void slotToNegative();
    void slotToNothing();

  public slots:
    //! Sets the state of the positive key.
    void changePositive(bool pressed);
    //! Sets the state of the negative key.
    void changeNegative(bool pressed);

  signals:
    void setPositive();
    void clearPositive();
    void setNegative();
    void clearNegative();

    //! Fires when positive state is current.
    void changedToPositive();
    //! Fires when negative state is current.
    void changedToNegative();
    //! Fires when zero state is current.
    void changedToNothing();
};

}

#endif
