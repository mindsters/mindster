#ifndef _CONTROLS_HPP
#define _CONTROLS_HPP

#include <QKeyEvent>

#include "GameKey.hpp"
#include "Control.hpp"
#include "KeyAutomaton.hpp"
#include "BlinkDetector.hpp"
#include "DoubleActionDetector.hpp"
#include "../persistance/Settings.hpp"

namespace gui {
class Window;
class GameActivity;
}

namespace input {

class Keyboard;
class ThinkGear;

/*!
 * Controller of the model.
 */
class Controls : public QObject {
    Q_OBJECT

  private:
    //! Holds keyboard instance which is processing events from keyboard input.
    Keyboard * keyboard;
    //! Automaton for resolving signal firing of elevation commands of the left player.
    KeyAutomaton * plL_elevation;
    //! Automaton for resolving signal firing of elevation commands of the right player.
    KeyAutomaton * plR_elevation;
    //! Automaton for resolving signal firing of movement commands of the left player.
    KeyAutomaton * plL_movement;
    //! Automaton for resolving signal firing of movement commands of the right player.
    KeyAutomaton * plR_movement;
    ThinkGear * player1_thinkGear;
    ThinkGear * player2_thinkGear;
    BlinkDetector blinkDetector1;
    BlinkDetector blinkDetector2;
    DoubleActionDetector doubleBlinkDetector1;
    DoubleActionDetector doubleBlinkDetector2;

  public:
    Controls(persistance::Settings * settings);
    ~Controls();
    //! Sets the keyboard instance as processor of keyboard events.
    void setKeyboardAsProcessor(gui::Window * window);
    //! Connects paused and start signals.
    void setGameActivitySignals(gui::GameActivity * activity);
    //! Return pointer to ThinkGear of player 1
    ThinkGear * getThinkGear_player1();
    //! Return pointer to ThinkGear of player 2
    ThinkGear * getThinkGear_player2();

  private slots:
    void plL_mv_still();
    void plL_mv_forward();
    void plL_mv_backward();

    void plL_el_stop();
    void plL_el_raise();
    void plL_el_lower();

    void plR_mv_still();
    void plR_mv_forward();
    void plR_mv_backward();

    void plR_el_stop();
    void plR_el_raise();
    void plR_el_lower();

    void plL_blink();
    void plR_blink();

    void plL_dblink();
    void plR_dblink();

  public slots:
    void fromKeyboard(GameKey gameKey, bool pressed);
    void fromSettings(QString player1, QString player2);

  signals:
    void plL_left(bool pressed);
    void plL_right(bool pressed);
    void plL_up(bool pressed);
    void plL_down(bool pressed);

    void plR_left(bool pressed);
    void plR_right(bool pressed);
    void plR_up(bool pressed);
    void plR_down(bool pressed);

    //! Fires the elevation command for the left player.
    void playerL_elevate(input::Elevation elevation);
    //! Fires the movement command for the left player.
    void playerL_move(input::Movement movement);
    //! Fires the shooting command for the left player;
    void playerL_shoot();
    //! Fires the elevation command for the right player.
    void playerR_elevate(input::Elevation elevation);
    //! Fires the movement command for the right player.
    void playerR_move(input::Movement movement);
    //! Fires the shooting command for the right player;
    void playerR_shoot();

    //! Fires new instances of ThinkGear on change
    void thinkGearsChanged(input::ThinkGear * player1, input::ThinkGear * player2);
};

}

#endif // _CONTROLS_HPP
