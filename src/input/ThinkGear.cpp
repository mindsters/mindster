#include "ThinkGear.hpp"

#define SYNC 0xAA
#define POOR_SIGNAL 0x02
#define HEART_RATE 0x03
#define ATTENTION 0x04
#define MEDITATION 0x05
#define RAW 0x80

using namespace input;

ThinkGear::ThinkGear(const QString & portName) {
  payloadSize = -1;

  serial = new QSerialPort(portName);

  serial->setBaudRate(QSerialPort::Baud9600);
  serial->setDataBits(QSerialPort::Data8);

  connect(serial, SIGNAL(readyRead()), this, SLOT(readData()));

  if (!serial->open(QIODevice::ReadOnly)) {
    QMessageBox::critical(0, "Error", serial->errorString());
  }
}

ThinkGear::~ThinkGear() {
  if (serial->isOpen())
    serial->close();

  delete serial;
}

void ThinkGear::readData() {
  QByteArray data = serial->readAll();
  unsigned char * bytes = (unsigned char *) data.data();

  for (int i = 0; i < data.size(); i++) {
    if (sizeNext) {
      payloadSize = bytes[i];
      byteCount = 0;
      sizeNext = false;
    } else if (bytes[i] == SYNC && buffer[byteCount - 1] == SYNC) {
      // if we get 2 SYNC bytes in a row, the next byte will be payload size
      sizeNext = true;
    } else {
      buffer[byteCount++] = bytes[i];

      if (byteCount == payloadSize + 1) {
        parsePacket(std::vector<unsigned char>(buffer, buffer + byteCount));
      }
    }
  }
}

/*
 * Accepts a payload of a Thinkgear packet and parses it. The last byte int the
 * payload has to be the checksum.
 */
void ThinkGear::parsePacket(std::vector<unsigned char> payload) {
  // calculate checksum
  int checksum = 0;

  for (int i = 0; i < payload.size() - 1; i++) {
    checksum += payload[i];
  }

  checksum &= 0xFF;
  checksum = ~checksum & 0xFF;

  // if checksum is wrong, don't parse the packet
  if (checksum != payload[payload.size() - 1])
    return;

  int i = 0;

  while (i < payload.size() - 2) {
    unsigned char code = payload[i];

    if (code >= RAW) {
      // multibyte packet
      int size = payload[++i];

      switch (code) {
        case RAW: {
            qint16 value = payload[++i] * 256; // higher byte
            value += payload[++i];             // lower byte
            emit raw(value);
            break;
          }

        default:
          i += size; // we don't care about this data, so we skip it
      }
    } else {
      // single byte packet
      switch (payload[i]) {
        case POOR_SIGNAL:
          emit poorSignal(payload[++i]);
          break;

        case HEART_RATE:
          emit heartRate(payload[++i]);
          break;

        case ATTENTION:
          emit attention(payload[++i]);
          break;

        case MEDITATION:
          emit meditation(payload[++i]);
          break;

        default:
          i++;
      }
    }

    i++;
  }
}
