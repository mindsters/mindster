#ifndef _CONTROL_HPP
#define _CONTROL_HPP

#include <QMetaEnum>

namespace input {

/*!
 * Elevation of the gun barrel.
 */
enum Elevation {
  LOWER,
  RAISE,
  STOP
};

/*!
 * Movement of the tank.
 */
enum Movement {
  FORWARD,
  BACKWARD,
  STILL
};

Q_ENUMS(Elevation)
Q_ENUMS(Movement)

}

#endif