#include <QDebug>

#include "Controls.hpp"
#include "Keyboard.hpp"
#include "ThinkGear.hpp"
#include "../gui/Window.hpp"
#include "../gui/GameActivity.hpp"

input::Controls::Controls(persistance::Settings * settings) : plL_elevation(new input::KeyAutomaton()), plR_elevation(new input::KeyAutomaton()), plL_movement(new input::KeyAutomaton()), plR_movement(new input::KeyAutomaton()), blinkDetector1(), blinkDetector2(), doubleBlinkDetector1(), doubleBlinkDetector2() {
  QObject::connect(settings, SIGNAL(portNamesSet(QString, QString)), this, SLOT(fromSettings(QString, QString)));
  keyboard = new Keyboard(this, settings);

  QObject::connect(this, SIGNAL(plL_right(bool)), plL_movement, SLOT(changePositive(bool)));
  QObject::connect(this, SIGNAL(plL_left(bool)), plL_movement, SLOT(changeNegative(bool)));
  QObject::connect(plL_movement, SIGNAL(changedToNothing()), this, SLOT(plL_mv_still()));
  QObject::connect(plL_movement, SIGNAL(changedToPositive()), this, SLOT(plL_mv_forward()));
  QObject::connect(plL_movement, SIGNAL(changedToNegative()), this, SLOT(plL_mv_backward()));

  QObject::connect(this, SIGNAL(plL_up(bool)), plL_elevation, SLOT(changePositive(bool)));
  QObject::connect(this, SIGNAL(plL_down(bool)), plL_elevation, SLOT(changeNegative(bool)));
  QObject::connect(plL_elevation, SIGNAL(changedToNothing()), this, SLOT(plL_el_stop()));
  QObject::connect(plL_elevation, SIGNAL(changedToPositive()), this, SLOT(plL_el_raise()));
  QObject::connect(plL_elevation, SIGNAL(changedToNegative()), this, SLOT(plL_el_lower()));

  QObject::connect(this, SIGNAL(plR_left(bool)), plR_movement, SLOT(changePositive(bool)));
  QObject::connect(this, SIGNAL(plR_right(bool)), plR_movement, SLOT(changeNegative(bool)));
  QObject::connect(plR_movement, SIGNAL(changedToNothing()), this, SLOT(plR_mv_still()));
  QObject::connect(plR_movement, SIGNAL(changedToPositive()), this, SLOT(plR_mv_forward()));
  QObject::connect(plR_movement, SIGNAL(changedToNegative()), this, SLOT(plR_mv_backward()));

  QObject::connect(this, SIGNAL(plR_up(bool)), plR_elevation, SLOT(changePositive(bool)));
  QObject::connect(this, SIGNAL(plR_down(bool)), plR_elevation, SLOT(changeNegative(bool)));
  QObject::connect(plR_elevation, SIGNAL(changedToNothing()), this, SLOT(plR_el_stop()));
  QObject::connect(plR_elevation, SIGNAL(changedToPositive()), this, SLOT(plR_el_raise()));
  QObject::connect(plR_elevation, SIGNAL(changedToNegative()), this, SLOT(plR_el_lower()));

  player1_thinkGear = NULL;
  player2_thinkGear = NULL;
  QObject::connect(&blinkDetector1, SIGNAL(blink()), this, SLOT(plL_blink()));
  QObject::connect(&blinkDetector1, SIGNAL(blink()), &doubleBlinkDetector1, SLOT(triggerAction()));
  QObject::connect(&doubleBlinkDetector1, SIGNAL(triggerDoubleAction()), this, SLOT(plL_dblink()));
  QObject::connect(&blinkDetector2, SIGNAL(blink()), this, SLOT(plR_blink()));
  QObject::connect(&blinkDetector2, SIGNAL(blink()), &doubleBlinkDetector2, SLOT(triggerAction()));
  QObject::connect(&doubleBlinkDetector2, SIGNAL(triggerDoubleAction()), this, SLOT(plR_dblink()));
}

input::Controls::~Controls() {
  delete plL_elevation;
  delete plR_elevation;
  delete plL_movement;
  delete plR_movement;
  delete keyboard;
  delete player1_thinkGear;
  delete player2_thinkGear;
}

void input::Controls::setKeyboardAsProcessor(gui::Window * window) {
  window->setKeyEventProcessor(keyboard);
}

void input::Controls::fromKeyboard(GameKey gameKey, bool pressed) {
  switch (gameKey) {
    case GameKey::PL1_DOWN:
      emit plL_down(pressed);
      break;

    case GameKey::PL1_UP:
      emit plL_up(pressed);
      break;

    case GameKey::PL1_LEFT:
      emit plL_left(pressed);
      break;

    case GameKey::PL1_RIGHT:
      emit plL_right(pressed);
      break;

    case GameKey::PL2_DOWN:
      emit plR_down(pressed);
      break;

    case GameKey::PL2_UP:
      emit plR_up(pressed);
      break;

    case GameKey::PL2_LEFT:
      emit plR_left(pressed);
      break;

    case GameKey::PL2_RIGHT:
      emit plR_right(pressed);
      break;
  }
}

void input::Controls::fromSettings(QString player1, QString player2) {
  if (player1_thinkGear != NULL) {
    delete player1_thinkGear;
  }

  if (player2_thinkGear != NULL) {
    delete player2_thinkGear;
  }

  player1_thinkGear = new ThinkGear(player1);
  player2_thinkGear = new ThinkGear(player2);
  QObject::connect(player1_thinkGear, SIGNAL(raw(qint16)), &blinkDetector1, SLOT(signalData(qint16)));
  QObject::connect(player2_thinkGear, SIGNAL(raw(qint16)), &blinkDetector2, SLOT(signalData(qint16)));

  emit thinkGearsChanged(player1_thinkGear, player2_thinkGear);
}

void input::Controls::plL_mv_still() {
  emit playerL_move(input::Movement::STILL);
}
void input::Controls::plL_mv_forward() {
  emit playerL_move(input::Movement::FORWARD);
}
void input::Controls::plL_mv_backward() {
  emit playerL_move(input::Movement::BACKWARD);
}

void input::Controls::plL_el_stop() {
  emit playerL_elevate(input::Elevation::STOP);
}
void input::Controls::plL_el_raise() {
  emit playerL_elevate(input::Elevation::RAISE);
}
void input::Controls::plL_el_lower() {
  emit playerL_elevate(input::Elevation::LOWER);
}

void input::Controls::plR_mv_still() {
  emit playerR_move(input::Movement::STILL);
}
void input::Controls::plR_mv_forward() {
  emit playerR_move(input::Movement::FORWARD);
}
void input::Controls::plR_mv_backward() {
  emit playerR_move(input::Movement::BACKWARD);
}

void input::Controls::plR_el_stop() {
  emit playerR_elevate(input::Elevation::STOP);
}
void input::Controls::plR_el_raise() {
  emit playerR_elevate(input::Elevation::RAISE);
}
void input::Controls::plR_el_lower() {
  emit playerR_elevate(input::Elevation::LOWER);
}

void input::Controls::plL_blink() {
  qDebug() << "Left Blink";
}
void input::Controls::plR_blink() {
  qDebug() << "Right Blink";
}

void input::Controls::plL_dblink() {
  qDebug() << "Left Double Blink";
  emit playerL_shoot();
}
void input::Controls::plR_dblink() {
  qDebug() << "Right Double Blink";
  emit playerR_shoot();
}

input::ThinkGear * input::Controls::getThinkGear_player1() {
  return player1_thinkGear;
}

input::ThinkGear * input::Controls::getThinkGear_player2() {
  return player2_thinkGear;
}

void input::Controls::setGameActivitySignals(gui::GameActivity * activity) {
  QObject::connect(activity, SIGNAL(paused()), &doubleBlinkDetector1, SLOT(inputDisabled()));
  QObject::connect(activity, SIGNAL(started()), &doubleBlinkDetector1, SLOT(inputEnabled()));
  QObject::connect(activity, SIGNAL(paused()), &doubleBlinkDetector2, SLOT(inputDisabled()));
  QObject::connect(activity, SIGNAL(started()), &doubleBlinkDetector2, SLOT(inputEnabled()));
}
