#ifndef _BLINKDETECTOR_HPP
#define _BLINKDETECTOR_HPP

#include <QObject>

namespace input {

/*!
 * Adaptive blink detector.
 */
class BlinkDetector : public QObject {
    Q_OBJECT
  private:
    //! State for average analysing.
    bool decrease;
    //! Last value of average.
    qint16 lastAvgValue;
    //! Average signal value.
    qint16 avgValue;
    //! Threshold for blink.
    qint16 threshold;
    //! Ratio of threshold to the maximum of signal.
    static const qreal thresholdRatio;
    //! Coefficient of the exponential moving average.
    static const qreal steepness;
    //! Coefficient of the exponential moving average of threshold.
    static const qreal thresholdSteepness;
    //! Initial threshold for blink.
    static const qint16 initialThreshold;
  public:
    BlinkDetector();
  public slots:
    void signalData(qint16 value);
  signals:
    void blink();
};

}

#endif
