#include <QDebug>

#include "BlinkDetector.hpp"

input::BlinkDetector::BlinkDetector() : decrease(false), lastAvgValue(0), avgValue(0), threshold(initialThreshold) {
}

const qreal input::BlinkDetector::thresholdRatio = 0.5;
const qreal input::BlinkDetector::steepness = 0.01;
const qreal input::BlinkDetector::thresholdSteepness = 0.1;
const qint16 input::BlinkDetector::initialThreshold = 30;

void input::BlinkDetector::signalData(qint16 value) {
  //qDebug() << avgValue;

  if (avgValue > threshold) {
    //qDebug() << threshold << "<" << avgValue;
    if (!decrease && avgValue - lastAvgValue < 0) {
      decrease = true;
      // Exponential moving average
      //threshold = (qint16)(thresholdSteepness * lastAvgValue * thresholdRatio + (1 - thresholdSteepness) * threshold);
      emit blink();
    }
  } else {
    decrease = false;
  }

  lastAvgValue = avgValue;

  // Exponential moving average
  avgValue = (qint16)(steepness * value + (1 - steepness) * avgValue);
}
