#ifndef _MINDWAVE_HPP
#define _MINDWAVE_HPP

#include <vector>

#include <QObject>
#include <QString>
#include <QtGlobal>

#include <QMessageBox>

#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>

#define BUFF_SIZE 170

namespace input {
/*!
 * Parses input from a serial port of a ThinkGear device. Emits signals for
 * values parsed from the device output.
 */
class ThinkGear : public QObject {
    Q_OBJECT

  public:
    //! Starts an instance listening to the port specified by name.
    ThinkGear(const QString & portName);
    ~ThinkGear();

  signals:
    //! Signal quaility 0-255 (high value means bad quality).
    void poorSignal(unsigned char poorSignal);
    //! Heart rate 0-255.
    void heartRate(unsigned char heartRate);
    //! Attention 0-100.
    void attention(unsigned char attention);
    //! Meditation 0-100.
    void meditation(unsigned char meditation);
    //! Raw signal value.
    void raw(qint16 raw);

  private:
    //! Serial port object.
    QSerialPort * serial;
    //! Buffer for input bytes.
    unsigned char buffer[BUFF_SIZE];
    //! Number of bytes currently in buffer.
    unsigned char byteCount;
    //! Size of the packet being parsed.
    unsigned char payloadSize;
    //! Size of the next expected packet.
    bool sizeNext;

    /*!
     * Parse a ThinkGear packet. The last byte of the input is expected
     * to be the checksum.
     */
    void parsePacket(std::vector<unsigned char> payload);

  private slots:
    //! Reacts to received data.
    void readData();
};
}

#endif //_MINDWAVE_HPP
