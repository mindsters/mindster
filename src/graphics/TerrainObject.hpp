#ifndef _TERRAINOBJECT_HPP
#define _TERRAINOBJECT_HPP

#include "GameObject.hpp"

namespace graphics {

/*!
 * Graphical repressentation of game terrain.
 */
class TerrainObject : public GameObject {
  public:
    //! Creates terrain with the given texture.
    TerrainObject(const QString fileName);
    //! Paints textured terrain.
    void paint(QPainter * painter);
  private:
    //! Brush used to fill the terrain polygon.
    QBrush brush;
};

}

#endif
