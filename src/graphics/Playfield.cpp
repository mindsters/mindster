#include <QDebug>
#include <QRect>
#include <QSize>
#include <QPen>

#include "Playfield.hpp"
#include "../model/GameState.hpp"
#include "../gui/GameActivity.hpp"

graphics::Playfield::Playfield(const QWidget * surface) : surface(surface), model(NULL), m_field(-50, -50, 100, 100), healthTank1(), healthTank2(), attentionP1(), attentionP2() {
  QObject::connect(this, SIGNAL(changed()), surface, SLOT(repaint()));
  busy = false;
}

void graphics::Playfield::setModel(const model::GameState * model) {
  this->model = model;
  QObject::connect(this->model, SIGNAL(changedState()), this, SLOT(repaint()));
  QObject::connect(this->model, SIGNAL(finished()), this, SLOT(repaint()));
  QObject::connect(model->getTank1(), SIGNAL(healthChanged(int)), this, SLOT(changeTank1Health(int)));
  QObject::connect(model->getTank2(), SIGNAL(healthChanged(int)), this, SLOT(changeTank2Health(int)));
}

void graphics::Playfield::clearModel() {
  this->model = NULL;
}

void graphics::Playfield::repaint() {
  QObject::disconnect(this->model, SIGNAL(changedState()), this, SLOT(repaint()));
  emit changed();
  QObject::connect(this->model, SIGNAL(changedState()), this, SLOT(repaint()));
}

void graphics::Playfield::setGeometry(const QSize & geometry) {
  QRect rect = QRect(geometry.width() / -2, geometry.height() / -2, geometry.width(), geometry.height());
  m_field = rect;

  int width = 100;
  int height = m_field.height() / 20;

  //Health bar tank1
  healthTank1.setX(10);
  healthTank1.setY(10);
  healthTank1.setHeight(height);
  healthTank1.setWidth(width);
  //Health bar tank2
  healthTank2.setX(m_field.width() - width - 10);
  healthTank2.setY(10);
  healthTank2.setHeight(height);
  healthTank2.setWidth(width);

  //Attention Player1
  attentionP1.setX(10);
  attentionP1.setY(height + 20);
  attentionP1.setHeight(height / 2);
  attentionP1.setWidth(width);

  //Attention Playet2
  attentionP2.setX(m_field.width() - width - 10);
  attentionP2.setY(height + 20);
  attentionP2.setHeight(height / 2);
  attentionP2.setWidth(width);
}

void graphics::Playfield::drawScene(QPainter * painter) {
  if (busy)
    return;

  busy = true;

  painter->fillRect(field(), QColor("light blue"));

  if (model) {
    model->representation()->paint(painter);
  }

  busy = false;
}

void graphics::Playfield::drawHUD(QPainter * painter, const QRect & area) {
  painter->setPen(QPen(Qt::red));
  painter->drawRect(0, 0, area.width() - 1, area.height() - 1);

  int width = 100;
  int height = m_field.height() / 20;

  painter->setPen(QColor(0, 0, 0));

  //healthTank1.setSize();
  painter->fillRect(healthTank1, QColor(250, 10, 10));
  painter->drawRect(10, 10, width, height);

  painter->fillRect(healthTank2, QColor(250, 10, 10));
  painter->drawRect(area.width() - width - 10, 10, width, height);

  attentionP1.setWidth(model->getAttentionP1() * 100);
  painter->fillRect(attentionP1, QColor(200, 150, 10));
  painter->drawRect(10, height + 20, width, height / 2);

  attentionP2.setWidth(model->getAttentionP2() * 100);
  painter->fillRect(attentionP2, QColor(200, 150, 10));
  painter->drawRect(area.width() - width - 10, height + 20, width, height / 2);

}

QRect graphics::Playfield::field() const {
  return m_field;
}

void graphics::Playfield::changeTank1Health(int health) {
  healthTank1.setWidth(health);
}

void graphics::Playfield::changeTank2Health(int health) {
  healthTank2.setWidth(health);
}
