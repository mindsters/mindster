#ifndef _SIMPLEOBJECT_HPP
#define _SIMPLEOBJECT_HPP

#include <QColor>

#include "GameObject.hpp"

namespace graphics {

class SimpleObject : public GameObject {
  private:
    QColor color;
  public:
    SimpleObject(QColor color);
    virtual void paint(QPainter * painter);
};

}

#endif