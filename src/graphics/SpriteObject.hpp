#ifndef _SPRITEOBJECT_HPP
#define _SPRITEOBJECT_HPP

#include <QImage>

#include "GameObject.hpp"

namespace graphics {

/*!
 * Class for graphical object of sprite.
 */
class SpriteObject : public GameObject {
  private:
    //! Image to be painted on given polygon which represent sprite object.
    QImage image;
    //! Color of polygon which will be painted if image failed to load.
    const QColor color;
  public:
    /*! Creates sprite object and image which is mirrored because of inverted y-axis.
     * \param fileName path to image file
     * \param color color of polygon which will be painted if image failed to load
     * \param horizontalMirror true if image should be mirrored horizontally
     */
    SpriteObject(const QString fileName, const QColor color, bool horizontalMirror = false);
    //! Paints the sprite object.
    virtual void paint(QPainter * painter);
    //! Sets model for this sprite object and scale image based on given model.
    virtual void setModel(const model::Body * model);
};

}

#endif // _GUNBARRELOBJECT_HPP
