#include "GameObject.hpp"

graphics::GameObject::GameObject() : model(NULL) {
}

void graphics::GameObject::setModel(const model::Body * model) {
  this->model = model;
}