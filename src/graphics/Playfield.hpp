#ifndef _PLAYFIELD_HPP
#define _PLAYFIELD_HPP

#include <QVector>
#include <QPainter>
#include <QObject>
#include <QWidget>
#include <QSize>

#include "GraphicalRepresentation.hpp"

namespace model {
class GameState;
}

namespace graphics {

/*!
 * Renderer of the game's field.
 */
class Playfield : public QObject {
    Q_OBJECT
  private:
    //! Holds the surface onto which is painted.
    const QWidget * surface;
    //! Tank1 health
    QRectF healthTank1;
    //! Tank1 health
    QRectF healthTank2;
    //! Player1 attention
    QRectF attentionP1;
    //! Player2 attention
    QRectF attentionP2;
    //! Holds the model which is rendered.
    const model::GameState * model;
    //! Physical geometry.
    QRect m_field;
    //! Indicates that the playfield is busy repainting.
    bool busy;
  private slots:
    //! Should Repaint the scene.
    void repaint();
  public slots:
    //! Sets the physical geometry of the field.
    void setGeometry(const QSize & geometry);
    //! Changes displayed health of tank1
    void changeTank1Health(int health);
    //! Changes displayed health of tank2
    void changeTank2Health(int health);
  public:
    Playfield(const QWidget * surface);
    //! Sets the model for the scene.
    void setModel(const model::GameState * model);
    //! Clears the model.
    void clearModel();
    //! Renders the scene.
    void drawScene(QPainter * painter);
    //! Renders the HUD.
    void drawHUD(QPainter * painter, const QRect & area);
    //! Returns the physical geometry of the field.
    QRect field() const;
  signals:
    //! Fires when the model of the scene is changed.
    void changed();
};

}

#endif
