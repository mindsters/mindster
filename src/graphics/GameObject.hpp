#ifndef _GAMEOBJECT_HPP
#define _GAMEOBJECT_HPP

#include <QPainter>

namespace model {
class Body;
}

namespace graphics {

/*!
 * Graphical object in a game.
 */
class GameObject {
  protected:
    //! Model of this graphical object.
    const model::Body * model;
  public:
    GameObject();
    virtual ~GameObject() = default;
    //! Paints this object.
    virtual void paint(QPainter * painter) = 0;
    //! Sets model for this graphical object.
    virtual void setModel(const model::Body * model);
};

}

#endif
