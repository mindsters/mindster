#include "TerrainObject.hpp"
#include "../model/Body.hpp"

using namespace graphics;

TerrainObject::TerrainObject(const QString fileName) :
  brush(QImage(fileName))
{}

void TerrainObject::paint(QPainter * painter) {
  QPolygonF shape = model->emplace();

  QPainterPath path;
  path.addPolygon(shape);

  painter->fillPath(path, brush);
}
