#include <QColor>

#include "GraphicalRepresentation.hpp"
#include "TerrainObject.hpp"
#include "SimpleObject.hpp"
#include "SpriteObject.hpp"

graphics::GraphicalRepresentation::GraphicalRepresentation() :
  m_tankL(new SpriteObject("./images/Tank2-body-small.png", QColor(255, 0, 0))),
  m_tankR(new SpriteObject("./images/Tank1-body-small.png", QColor(0, 0, 255))),
  m_tankL_gun(new SpriteObject("./images/Tank2-gun-small.png", QColor(255, 0, 0))),
  m_tankR_gun(new SpriteObject("./images/Tank1-gun-small.png", QColor(0, 0, 255))),
  m_terrain(new TerrainObject("./images/terrain_small.png")),
  objects()
{ }

graphics::GraphicalRepresentation::~GraphicalRepresentation() {
  delete m_tankL_gun;
  delete m_tankR_gun;
  delete m_tankL;
  delete m_tankR;
  delete m_terrain;
}

void graphics::GraphicalRepresentation::paint(QPainter * painter) {
  m_terrain->paint(painter);
  m_tankL_gun->paint(painter);
  m_tankR_gun->paint(painter);
  m_tankL->paint(painter);
  m_tankR->paint(painter);

  for (auto object = objects.begin(); object != objects.end(); object++) {
    (*object)->paint(painter);
  }
}

graphics::GameObject * graphics::GraphicalRepresentation::tankL() {
  return m_tankL;
}

graphics::GameObject * graphics::GraphicalRepresentation::tankR() {
  return m_tankR;
}

graphics::GameObject * graphics::GraphicalRepresentation::tankL_gun() {
  return m_tankL_gun;
}

graphics::GameObject * graphics::GraphicalRepresentation::tankR_gun() {
  return m_tankR_gun;
}

graphics::GameObject * graphics::GraphicalRepresentation::terrain() {
  return m_terrain;
}

graphics::GameObject * graphics::GraphicalRepresentation::createGraphicalBullet() {
  graphics::GameObject * bullet = new graphics::SpriteObject("./images/projectile-small.png", QColor("red"));
  objects.append(bullet);
  return bullet;
}

void graphics::GraphicalRepresentation::destroyGraphicalBullet(graphics::GameObject * bullet) {
  objects.removeOne(bullet);
  delete bullet;
}
