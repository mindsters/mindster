#ifndef _GRAPHICALREPRESENTATION_HPP
#define _GRAPHICALREPRESENTATION_HPP

#include <QList>
#include <QPainter>

#include "GameObject.hpp"

namespace graphics {

/*!
 * The intermediate class used by model for connecting to the graphical objects and by graphics for rendering
 */
class GraphicalRepresentation {
  private:
    //! Holds the graphical tank object for the left player.
    GameObject * m_tankL;
    //! Holds the graphical tank object for the right player.
    GameObject * m_tankR;
    //! Holds the graphical tank gun object for the left player.
    GameObject * m_tankL_gun;
    //! Holds the graphical tank gun object for the right player.
    GameObject * m_tankR_gun;
    //! Graphical representation of terrain.
    GameObject * m_terrain;
    //! Holds graphical objects.
    QList<GameObject *> objects;
  public:
    GraphicalRepresentation();
    ~GraphicalRepresentation();
    //! Renders all graphical objects.
    void paint(QPainter * painter);
    //! Accessor of the graphical tank object for the left player.
    GameObject * tankL();
    //! Accessor of the graphical tank object for the right player.
    GameObject * tankR();
    //! Accessor of the graphical tank gun object for the left player.
    GameObject * tankL_gun();
    //! Accessor of the graphical tank gun object for the right player.
    GameObject * tankR_gun();
    //! Accessor of the graphical terrain object.
    GameObject * terrain();
    //! Creates graphical bullet object.
    GameObject * createGraphicalBullet();
    //! Destroys graphical bullet object.
    void destroyGraphicalBullet(GameObject * bullet);
};

}

#endif
