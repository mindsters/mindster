#include "SpriteObject.hpp"
#include "../model/Body.hpp"

using namespace graphics;

SpriteObject::SpriteObject(const QString fileName, const QColor color, bool horizontalMirror) : image(fileName), color(color) {
  image = image.mirrored(horizontalMirror, true); //y-axis are inverted
}

void SpriteObject::paint(QPainter * painter) {
  QPolygonF shape = model->emplace();

  if (image.isNull()) {
    QPainterPath path;
    path.addPolygon(shape);
    painter->fillPath(path, QBrush(color));
  } else {
    QTransform t;
    t.rotateRadians(model->angle());
    QImage paintImage = image.transformed(t);
    QRectF rect = shape.boundingRect();
    painter->drawImage(rect, paintImage);
  }
}

void SpriteObject::setModel(const model::Body * model) {
  if (image.isNull()) {
    return;
  }

  GameObject::setModel(model);
  QRect rect = model->emplace().boundingRect().toRect();
  image = image.scaled(rect.size());
}
