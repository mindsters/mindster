#include <QDebug>
#include <QPolygonF>
#include <QBrush>
#include <QPainterPath>

#include "SimpleObject.hpp"
#include "../model/Body.hpp"

graphics::SimpleObject::SimpleObject(QColor color) : color(color) {
}

void graphics::SimpleObject::paint(QPainter * painter) {
  QPolygonF shape = model->emplace();
  QPainterPath path;
  path.addPolygon(shape);
  painter->fillPath(path, QBrush(color));
  /*qDebug() << "Color: " << color;
  qDebug() << "Position: " << model->position();
  qDebug() << "Angle: " << model->angle();*/
}