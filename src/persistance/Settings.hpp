#ifndef _SETTINGS_HPP
#define _SETTINGS_HPP

#include "JSONSerializer.hpp"

namespace persistance {

class Settings : public QObject {
    Q_OBJECT

  private:
    JSONSerializer * serializer;
    Qt::Key player1_moveLeft, player1_moveRight, player1_gunBarrelUp, player1_gunBarrelDown,
    player2_moveLeft, player2_moveRight, player2_gunBarrelUp, player2_gunBarrelDown;
    QString player1_portName, player2_portName;

  public:
    Settings(JSONSerializer * serializer);

    Qt::Key getPlayer1_moveLeft();
    Qt::Key getPlayer1_moveRight();
    Qt::Key getPlayer1_gunBarrelUp();
    Qt::Key getPlayer1_gunBarrelDown();
    Qt::Key getPlayer2_moveLeft();
    Qt::Key getPlayer2_moveRight();
    Qt::Key getPlayer2_gunBarrelUp();
    Qt::Key getPlayer2_gunBarrelDown();

    QString getPlayer1_portName();
    QString getPlayer2_portName();

    void setPlayer1_moveLeft(Qt::Key key);
    void setPlayer1_moveRight(Qt::Key key);
    void setPlayer1_gunBarrelUp(Qt::Key key);
    void setPlayer1_gunBarrelDown(Qt::Key key);
    void setPlayer2_moveLeft(Qt::Key key);
    void setPlayer2_moveRight(Qt::Key key);
    void setPlayer2_gunBarrelUp(Qt::Key key);
    void setPlayer2_gunBarrelDown(Qt::Key key);

    void setPortNames(QString player1, QString player2);

    void load();
    void save();

  signals:
    void portNamesSet(QString player1, QString player2);
};

}

#endif
