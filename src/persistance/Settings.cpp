#include "Settings.hpp"
#include "JSONSerializer.hpp"

persistance::Settings::Settings(JSONSerializer * serializer) : serializer(serializer) {
  setPlayer1_moveLeft(Qt::Key_A);
  setPlayer1_moveRight(Qt::Key_D);
  setPlayer1_gunBarrelUp(Qt::Key_W);
  setPlayer1_gunBarrelDown(Qt::Key_S);

  setPlayer2_moveLeft(Qt::Key_Left);
  setPlayer2_moveRight(Qt::Key_Right);
  setPlayer2_gunBarrelUp(Qt::Key_Up);
  setPlayer2_gunBarrelDown(Qt::Key_Down);
}

Qt::Key persistance::Settings::getPlayer1_moveLeft() {
  return player1_moveLeft;
}

Qt::Key persistance::Settings::getPlayer1_moveRight() {
  return player1_moveRight;
}

Qt::Key persistance::Settings::getPlayer1_gunBarrelUp() {
  return player1_gunBarrelUp;
}

Qt::Key persistance::Settings::getPlayer1_gunBarrelDown() {
  return player1_gunBarrelDown;
}

Qt::Key persistance::Settings::getPlayer2_moveLeft() {
  return player2_moveLeft;
}

Qt::Key persistance::Settings::getPlayer2_moveRight() {
  return player2_moveRight;
}

Qt::Key persistance::Settings::getPlayer2_gunBarrelUp() {
  return player2_gunBarrelUp;
}

Qt::Key persistance::Settings::getPlayer2_gunBarrelDown() {
  return player2_gunBarrelDown;
}

QString persistance::Settings::getPlayer1_portName() {
  return player1_portName;
}

QString persistance::Settings::getPlayer2_portName() {
  return player2_portName;
}

void persistance::Settings::setPlayer1_moveLeft(Qt::Key key) {
  if (key == player1_moveRight || key == player1_gunBarrelUp || key == player1_gunBarrelDown || key == player2_moveLeft
      || key == player2_moveRight || key == player2_gunBarrelUp || key == player2_gunBarrelDown || key == Qt::Key_Escape) {
    return;
  }

  player1_moveLeft = key;
}

void persistance::Settings::setPlayer1_moveRight(Qt::Key key) {
  if (key == player1_moveLeft || key == player1_gunBarrelUp || key == player1_gunBarrelDown || key == player2_moveLeft
      || key == player2_moveRight || key == player2_gunBarrelUp || key == player2_gunBarrelDown || key == Qt::Key_Escape) {
    return;
  }

  player1_moveRight = key;
}

void persistance::Settings::setPlayer1_gunBarrelUp(Qt::Key key) {
  if (key == player1_moveLeft || key == player1_moveRight || key == player1_gunBarrelDown || key == player2_moveLeft
      || key == player2_moveRight || key == player2_gunBarrelUp || key == player2_gunBarrelDown || key == Qt::Key_Escape) {
    return;
  }

  player1_gunBarrelUp = key;
}

void persistance::Settings::setPlayer1_gunBarrelDown(Qt::Key key) {
  if (key == player1_moveLeft || key == player1_moveRight || key == player1_gunBarrelUp || key == player2_moveLeft
      || key == player2_moveRight || key == player2_gunBarrelUp || key == player2_gunBarrelDown || key == Qt::Key_Escape) {
    return;
  }

  player1_gunBarrelDown = key;
}

void persistance::Settings::setPlayer2_moveLeft(Qt::Key key) {
  if (key == player1_moveLeft || key == player1_moveRight || key == player1_gunBarrelUp || key == player1_gunBarrelDown
      || key == player2_moveRight || key == player2_gunBarrelUp || key == player2_gunBarrelDown || key == Qt::Key_Escape) {
    return;
  }

  player2_moveLeft = key;
}

void persistance::Settings::setPlayer2_moveRight(Qt::Key key) {
  if (key == player1_moveLeft || key == player1_moveRight || key == player1_gunBarrelUp || key == player1_gunBarrelDown
      || key == player2_moveLeft || key == player2_gunBarrelUp || key == player2_gunBarrelDown || key == Qt::Key_Escape) {
    return;
  }

  player2_moveRight = key;
}

void persistance::Settings::setPlayer2_gunBarrelUp(Qt::Key key) {
  if (key == player1_moveLeft || key == player1_moveRight || key == player1_gunBarrelUp || key == player1_gunBarrelDown
      || key == player2_moveLeft || key == player2_moveRight || key == player2_gunBarrelDown || key == Qt::Key_Escape) {
    return;
  }

  player2_gunBarrelUp = key;
}

void persistance::Settings::setPlayer2_gunBarrelDown(Qt::Key key) {
  if (key == player1_moveLeft || key == player1_moveRight || key == player1_gunBarrelUp || key == player1_gunBarrelDown
      || key == player2_moveLeft || key == player2_moveRight || key == player2_gunBarrelUp || key == Qt::Key_Escape) {
    return;
  }

  player2_gunBarrelDown = key;
}

void persistance::Settings::setPortNames(QString player1, QString player2) {
  player1_portName = player1;
  player2_portName = player2;

  emit portNamesSet(player1, player2);
}

void persistance::Settings::save() {

}

void persistance::Settings::load() {

}
