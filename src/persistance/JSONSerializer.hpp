#ifndef _JSONSERIALIZER_HPP
#define _JSONSERIALIZER_HPP

#include <QFile>
#include <QString>

namespace persistance {

class Settings;

class JSONSerializer {
  private:
    QFile file;
  public:
    JSONSerializer(const QString & filename);
    bool save(const Settings * settings);
    bool load(Settings * settings);
};

}

#endif
