#include "MovingBody.hpp"

using namespace model;

MovingBody::MovingBody(const QPointF & position, qreal angle,
                       const QPolygonF & shape, graphics::GameObject * gameObject,
                       const QPointF & rotateCenter, const QVector2D & initialVelocity) :
  Body(position, angle, shape, gameObject, rotateCenter),
  m_velocity(initialVelocity)
{ }

QVector2D MovingBody::velocity() {
  return m_velocity;
}

void MovingBody::setVelocity(QVector2D velocity) {
  m_velocity = velocity;
}
