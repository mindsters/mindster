#ifndef _BULLET_HPP
#define _BULLET_HPP

#include <QVector2D>

#include "MovingBody.hpp"

namespace model {

class Bullet : public MovingBody {
  private:
    graphics::GameObject * m_gameObject;
    //! Bullet damage
    int m_damage;
  public:
    Bullet(graphics::GameObject * gameObject, QVector2D initialVelocity, int damage);
    graphics::GameObject * graphicalBullet();
    void setVelocity(QVector2D velocity);
    int damage();
};

}

#endif
