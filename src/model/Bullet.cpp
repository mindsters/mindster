#include <math.h>
#include <QTransform>

#include "Bullet.hpp"

model::Bullet::Bullet(graphics::GameObject * gameObject, QVector2D initialVelocity, int damage) :
  MovingBody(QPointF(0, 0), 0,
             QPolygonF()
             << QPointF(-10, -5) << QPointF(10, -5)
             << QPointF(10, 5) << QPointF(-10, 5),
             gameObject, QPointF(0, 0), initialVelocity),
  m_gameObject(gameObject) {
  m_damage = damage;
}

graphics::GameObject * model::Bullet::graphicalBullet() {
  return m_gameObject;
}

void model::Bullet::setVelocity(QVector2D velocity) {
  qreal turn = atan2(velocity.y(), velocity.x());
  MovingBody::rotate(turn - MovingBody::angle());
  MovingBody::setVelocity(velocity);
}

int model::Bullet::damage() {
  return m_damage;
}
