#ifndef _GAMESTATE_HPP
#define _GAMESTATE_HPP

#include <QElapsedTimer>
#include <QThread>
#include <QVector>
#include <QVector2D>
#include <QList>
#include <QReadWriteLock>

#include "Tank.hpp"
#include "Terrain.hpp"
#include "Bullet.hpp"
#include "GunBarrel.hpp"
#include "../input/Controls.hpp"
#include "../input/Control.hpp"
#include "../input/ThinkGear.hpp"

namespace graphics {
class GraphicalRepresentation;
class Playfield;
}

namespace model {

class GameState : public QThread {
    Q_OBJECT
  private:
    //! The minimal time of step duration in ms.
    static const int TIME_STEP;
    //! Gravity acceleration [px/s^2].
    static const QVector2D acceleration;
    //! True if the game is running.
    bool simulating;
    input::Controls * controls;
    graphics::GraphicalRepresentation * m_representation;
    QRectF world;
    //! Counting time when actions should not be done
    QElapsedTimer delayTime;
    QElapsedTimer realTime;
    //! Counts present delay
    qint64 cumulatedRealDelay;
    qint64 logicalTimepoint;
    QVector2D moveTank1;
    QVector2D moveTank2;
    Terrain * terrain;
    Tank * tank1;
    Tank * tank2;
    qreal rotateTank1_gun;
    qreal rotateTank2_gun;
    QList<Bullet *> bullets;
    QReadWriteLock bulletLock;
    //! Holds attention of player 1
    float attentionP1;
    //! Holds attention of player 2
    float attentionP2;
    //! Time for the previous step.
    int lastLogicalNow;
    //! Performs the step.
    bool step(int now, int diff);
    //! The game's loop.
    void run();
    //! Create a bullet.
    void createBullet(QVector2D position, qreal angle, qreal scalarVelocity, int damage);
    //! Destroy a bullet.
    void destroyBullet(Bullet * bullet);
  public:
    GameState(QRectF world, input::Controls * controls, graphics::GraphicalRepresentation * representation);
    ~GameState();
    //! Returns a graphical representation of the game model.
    graphics::GraphicalRepresentation * representation() const;
    //! Starts delayTime.
    void startDelayTime();
    //! Sets delayTime invalid and sets delay to 0.
    void stopDelayTime();
    //! Returns pointer to tank1
    Tank * getTank1() const;
    //! Returns pointer to tank2
    Tank * getTank2() const;
    float getAttentionP1() const;
    float getAttentionP2() const;

  public slots:
    //! Sets elevation of the left player.
    void playerL_elevate(input::Elevation elevation);
    //! Sets movement of the left player.
    void playerL_move(input::Movement movement);
    //! Shooting from the left player.
    void playerL_shoot();
    //! Sets elevation of the right player.
    void playerR_elevate(input::Elevation elevation);
    //! Sets movement of the right player.
    void playerR_move(input::Movement movement);
    //! Shooting from the right player.
    void playerR_shoot();
    //! Changes attention of player 1
    void attentionP1Changed(unsigned char attention);
    //! Changes attention of player 2
    void attentionP2Changed(unsigned char attention);
    //! Connects ThinkGears to GameState
    void connectThinkGears(input::ThinkGear * player1, input::ThinkGear * player2);

  signals:
    //! Fires when the game is over.
    void finished(QString str);
    //! Fires when the state of the world is changed.
    void changedState();
};

}

#endif
