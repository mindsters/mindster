#include "Tank.hpp"
#include "Bullet.hpp"

using namespace model;

Tank::Tank(graphics::GameObject * gameObject, GunBarrel * barrel) :
  MovingBody(QPointF(0, 0), 0,
             QPolygonF() << QPointF(-25, -37.5) << QPointF(25, -37.5) << QPointF(37.5, -15)
             << QPointF(0, 15) << QPointF(-37.5, -15) << QPointF(-25, -37.5), gameObject, QPointF(0, 0), QVector2D(0, 0)),
  m_barrel(barrel) {
  health = 100;
}

void Tank::subtractHealth(int damage) {
  health -= damage;

  if (health <= 0) {
    health = 0;
  }

  emit healthChanged(health);
}

int Tank::getHealth() {
  return health;
}

GunBarrel * Tank::barrel() {
  return m_barrel;
}

void Tank::tankHit(Bullet * bullet) {
  subtractHealth(bullet->damage() / 5); //needs to be changed to calculating bullet's damage
}

void Tank::move(const QVector2D & shift) {
  Body::move(shift);
  barrel()->move(shift);
}
