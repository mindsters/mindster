#ifndef _GUNBARREL_HPP
#define _GUNBARREL_HPP

#include "Body.hpp"

namespace model {

class GunBarrel : public Body {
  public:
    GunBarrel(QPointF m_position, QPointF rotateCenter, graphics::GameObject * gameObject);
};

}

#endif // _GUNBARREL_HPP