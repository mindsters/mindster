#include "Body.hpp"
#include "../graphics/GameObject.hpp"

#include <QTransform>

model::Body::Body(const QPointF & position, qreal angle, const QPolygonF & shape, graphics::GameObject * gameObject, const QPointF & rotateCenter) : m_position(position), m_rotateCenter(rotateCenter), m_angle(angle), shape(shape) {
  gameObject->setModel(this);
}

void model::Body::move(const QVector2D & shift) {
  m_position += shift.toPointF();
}

void model::Body::rotate(qreal turn) {
  m_angle += turn;
}

QPolygonF model::Body::emplace() const {
  QTransform t;
  //transform operations is done from last to first
  t.translate(m_position.x() - m_rotateCenter.x(), m_position.y() - m_rotateCenter.y());
  t.rotateRadians(m_angle);
  t.translate(m_rotateCenter.x(), m_rotateCenter.y());
  return t.map(shape);
}

QPointF model::Body::position() const {
  return m_position;
}

qreal model::Body::angle() const {
  return m_angle;
}

QPointF model::Body::rotateCenter() const {
  return m_rotateCenter;
}