#ifndef _TANK_HPP
#define _TANK_HPP

#include "MovingBody.hpp"
#include "GunBarrel.hpp"

namespace model {

class Bullet;

/*!
 * Class which represent tank's model.
 */
class Tank : public MovingBody {
    Q_OBJECT
  private:
    //! Tank's health
    int health;
    //! Pointer to tank's gun.
    GunBarrel * m_barrel;
  public:
    //! Creates tank's model and set health to maximum.
    Tank(graphics::GameObject * gameObject, GunBarrel * barrel);
    //! Returns number of health
    int getHealth();
    /*!
     * Subtract tank's health, emits signal healChanged with new health.
     */
    void subtractHealth(int damage);
    //! Call subtractHealth
    void tankHit(Bullet * bullet);
    //! Accessor for the gun barrel.
    GunBarrel * barrel();
    //! Translates the tank and its gun.
    virtual void move(const QVector2D & shift);
  signals:
    /*!
     * Emit when tank is hit
     * \param health new actual health
     */
    void healthChanged(int health);
};

}

#endif
