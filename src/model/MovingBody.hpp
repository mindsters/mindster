#ifndef _MOVINGBODY_HPP
#define _MOVINGBODY_HPP

#include "Body.hpp"

namespace model {

/*!
 * Class for model objects with a body.
 */
class MovingBody : public Body {
  private:
    //! Velocity [px/s]
    QVector2D m_velocity;
  public:
    MovingBody(const QPointF & position, qreal angle, const QPolygonF & shape, graphics::GameObject * gameObject, const QPointF & rotateCenter = QPointF(0, 0), const QVector2D & initialVelocity = QVector2D(0, 0));
    QVector2D velocity();
    void setVelocity(QVector2D velocity);
};

}

#endif
