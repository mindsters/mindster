#ifndef _TERRAIN_HPP
#define _TERRAIN_HPP


#include <QVector>
#include <QRectF>
#include <QPolygonF>

#include "Body.hpp"

namespace model {

/*!
 * Representation of game terrain.
 */
class Terrain: public Body {
    Q_OBJECT

  public:
    //! Create terrain from the game world.
    Terrain(const QRectF & world, graphics::GameObject * gameObject);
    //! Remove circular section of terrain determined by center point and radius.
    void removeCircle(QPointF center, float r);
    void addCircle(QPointF center, float r);
};

}

#endif
