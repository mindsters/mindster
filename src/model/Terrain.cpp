#include "Terrain.hpp"

#include <cmath>

using namespace model;

Terrain::Terrain(const QRectF & world, graphics::GameObject * gameObject) :
  Body(QPointF(0, 0), 0,
       QPolygonF()
       // top left
       << QPointF(world.bottomLeft().x(), 0)
       // top right
       << QPointF(world.bottomRight().x(), 0)
       // bottom right
       << world.topRight()
       // bottom left
       << world.topLeft(),
       gameObject) {
  addCircle(QPointF(0, 0), world.width() / 5);
}

void Terrain::removeCircle(QPointF center, float r) {
  QPolygonF hole;
  int steps = 20;

  for (int i = 0; i < steps; i++) {
    float angle = (float) i / steps * 2 * M_PI;
    hole << QPointF(center.x() + r * sin(angle),
                    center.y() + r * cos(angle));
  }

  shape = shape.subtracted(hole);
}

void Terrain::addCircle(QPointF center, float r) {
  QPolygonF circle;
  int steps = 20;

  for (int i = 0; i < steps; i++) {
    float angle = (float) i / steps * 2 * M_PI;
    circle << QPointF(center.x() + r * sin(angle),
                      center.y() + r * cos(angle));
  }

  shape = shape.united(circle);
}
