#include <QDebug>
#include <QVector2D>
#include <QPolygonF>
#include <QMutableListIterator>
#include <QTransform>
#include <QWriteLocker>

#include <math.h>

#include "GameState.hpp"
#include "../graphics/GraphicalRepresentation.hpp"
#include "../graphics/Playfield.hpp"

#define GUN_STEPRADIAN 0.01
#define GUN_MAXRADIAN 0.8 //for tank2 is max = -min
#define GUN_MINRADIAN -0.4 //for tank2 is min = -max

model::GameState::GameState(QRectF world, input::Controls * controls,
                            graphics::GraphicalRepresentation * representation) :
  simulating(true), controls(controls),
  m_representation(representation),
  world(world), delayTime(),
  realTime(), cumulatedRealDelay(0),
  logicalTimepoint(0), moveTank1(0, 0),
  moveTank2(0, 0), rotateTank1_gun(0),
  rotateTank2_gun(0), bullets(),
  bulletLock(),
  attentionP1(0), attentionP2(0) {
  QObject::connect(controls, SIGNAL(playerL_elevate(input::Elevation)),
                   this,     SLOT(playerL_elevate(input::Elevation)));
  QObject::connect(controls, SIGNAL(playerL_move(input::Movement)),
                   this,     SLOT(playerL_move(input::Movement)));
  QObject::connect(controls, SIGNAL(playerL_shoot()),
                   this,     SLOT(playerL_shoot()));
  QObject::connect(controls, SIGNAL(playerR_elevate(input::Elevation)),
                   this,     SLOT(playerR_elevate(input::Elevation)));
  QObject::connect(controls, SIGNAL(playerR_move(input::Movement)),
                   this,     SLOT(playerR_move(input::Movement)));
  QObject::connect(controls, SIGNAL(playerR_shoot()),
                   this,     SLOT(playerR_shoot()));
  QObject::connect(controls, SIGNAL(thinkGearsChanged(input::ThinkGear *, input::ThinkGear *)),
                   this,     SLOT(connectThinkGears(input::ThinkGear *, input::ThinkGear *)));

  if (controls->getThinkGear_player1() != NULL && controls->getThinkGear_player2() != NULL) {
    connectThinkGears(controls->getThinkGear_player1(), controls->getThinkGear_player2());
  }

  /*
  qDebug() << "World:";
  qDebug() << "BL: " << world.bottomLeft();
  qDebug() << "BR: " << world.bottomRight();
  qDebug() << "TL: " << world.topLeft();
  qDebug() << "TR: " << world.topRight();
  */

  terrain = new Terrain(world, representation->terrain());

  GunBarrel * tank1_gun = new GunBarrel(QPointF(33.57, -11.25), QPointF(16.5, -12.75), representation->tankL_gun());
  GunBarrel * tank2_gun = new GunBarrel(QPointF(-24.75, -7.5), QPointF(-16.5, -12.75), representation->tankR_gun());

  tank1 = new Tank(representation->tankL(), tank1_gun);
  tank1->move(QVector2D(world.left() + 40, 40)); // reverse y-axis
  tank2 = new Tank(representation->tankR(), tank2_gun);
  tank2->move(QVector2D(world.right() - 40, 40)); // reverse y-axis

  setTerminationEnabled();
  start();
}

model::GameState::~GameState() {
  terminate();
  delete tank1->barrel();
  delete tank2->barrel();
  delete tank1;
  delete tank2;
  delete terrain;
}

const int model::GameState::TIME_STEP = 32;//16;

const QVector2D model::GameState::acceleration = QVector2D(0, -20);

graphics::GraphicalRepresentation * model::GameState::representation() const {
  return m_representation;
}

void model::GameState::startDelayTime() {
  delayTime.restart();
}

void model::GameState::stopDelayTime() {
  cumulatedRealDelay += delayTime.elapsed();
  delayTime.invalidate();
}

bool model::GameState::step(int now, int diff) {
  qreal seconds = diff * 0.001;
  bool fallen = false;

  QPolygonF terrainShape = terrain->emplace();

  if (!world.contains(tank1->position())) {
    tank1->subtractHealth(tank1->getHealth());
    fallen = true;
  }

  if (!world.contains(tank2->position())) {
    tank2->subtractHealth(tank2->getHealth());
    fallen = true;
  }

  if (fallen) {
    return false;
  }

  if (world.contains(tank1->position() + 2 * moveTank1.toPointF())
      && !terrainShape.containsPoint(tank1->position() + moveTank1.toPointF(), Qt::OddEvenFill)) {
    tank1->move(moveTank1);
  }

  if (world.contains(tank2->position() + 2 * moveTank2.toPointF())
      && !terrainShape.containsPoint(tank2->position() + moveTank2.toPointF(), Qt::OddEvenFill)) {
    tank2->move(moveTank2);
  }

  if (GUN_MAXRADIAN > tank1->barrel()->angle() + rotateTank1_gun
      && GUN_MINRADIAN < tank1->barrel()->angle() + rotateTank1_gun) {
    tank1->barrel()->rotate(rotateTank1_gun);
  }

  if ((-GUN_MINRADIAN) > tank2->barrel()->angle() + rotateTank2_gun
      && (-GUN_MAXRADIAN) < tank2->barrel()->angle() + rotateTank2_gun) {
    tank2->barrel()->rotate(rotateTank2_gun);
  }

  // Tanks gravity
  if (!terrainShape.containsPoint(tank1->position() + QPoint(0, -37.5), Qt::OddEvenFill)) {
    QVector2D moveVector = tank1->velocity() * seconds;
    tank1->setVelocity(tank1->velocity() + acceleration * seconds);
    tank1->move(moveVector);
  }

  if (terrainShape.containsPoint(tank1->position() + QPoint(0, -33.5), Qt::OddEvenFill)) {
    tank1->move(QVector2D(0, 3));
  }

  if (!terrainShape.containsPoint(tank2->position() + QPoint(0, -37.5), Qt::OddEvenFill)) {
    QVector2D moveVector = tank2->velocity() * seconds;
    tank2->setVelocity(tank2->velocity() + acceleration * seconds);
    tank2->move(moveVector);
  }

  if (terrainShape.containsPoint(tank2->position() + QPoint(0, -33.5), Qt::OddEvenFill)) {
    tank2->move(QVector2D(0, 3));
  }

  QPolygonF shape1 = tank1->emplace();
  QPolygonF shape2 = tank2->emplace();

  // Bullet block of code
  {
    QWriteLocker locker(&bulletLock);

    QMutableListIterator<Bullet *> i(bullets);

    while (i.hasNext()) {
      Bullet * bullet = i.next();

      QVector2D moveVector = bullet->velocity() * seconds;
      bullet->setVelocity(bullet->velocity() + acceleration * seconds);
      bullet->move(moveVector);

      if (!world.contains(bullet->position())) {
        destroyBullet(bullet);
        i.remove();
      } else {
        QPolygonF shapeBullet = bullet->emplace();

        if (!terrainShape.intersected(shapeBullet).isEmpty()) {
          terrain->removeCircle(bullet->position(), bullet->damage());
          destroyBullet(bullet);
          i.remove();
        } else if (!shape1.intersected(shapeBullet).isEmpty()) {
          tank1->tankHit(bullet);
          destroyBullet(bullet);
          i.remove();

          if (tank1->getHealth() == 0) {
            return false;
          }
        } else if (!shape2.intersected(shapeBullet).isEmpty()) {
          tank2->tankHit(bullet);
          destroyBullet(bullet);
          i.remove();

          if (tank2->getHealth() == 0) {
            return false;
          }
        }
      }
    }
  }

  return true;
}

void model::GameState::run() {
  lastLogicalNow = 0;
  logicalTimepoint = 0;
  cumulatedRealDelay = 0;
  realTime.start();
  delayTime.start();
  delayTime.invalidate();

  while (simulating) {
    qint64 currentRealDelay = cumulatedRealDelay;

    if (delayTime.isValid()) {
      currentRealDelay += delayTime.elapsed();
    }

    if (realTime.hasExpired(logicalTimepoint + currentRealDelay)) {
      qint64 logicalNow = realTime.elapsed() - currentRealDelay;
      logicalTimepoint = logicalNow + model::GameState::TIME_STEP;
      simulating = step(logicalNow, logicalNow - lastLogicalNow);
      lastLogicalNow = logicalNow;
      emit changedState();
    }

    QThread::msleep(model::GameState::TIME_STEP);
  }

  QString str;

  if (tank1->getHealth() == 0) {
    if (tank2->getHealth() == 0) {
      str.append("Draw.");
    } else {
      str.append("Player 2 won.");
    }
  } else {
    str.append("Player 1 won.");
  }

  emit finished(str);
}

void model::GameState::createBullet(QVector2D position, qreal angle, qreal scalarVelocity, int damage) {
  QWriteLocker locker(&bulletLock);

  QPointF v = QVector2D(scalarVelocity, 0).toPointF();
  QTransform t;
  t.rotateRadians(angle);
  v = t.map(v);
  Bullet * bullet = new Bullet(representation()->createGraphicalBullet(), QVector2D(v), damage);
  bullet->move(position);
  bullet->rotate(angle);
  bullets.append(bullet);
}

void model::GameState::destroyBullet(Bullet * bullet) {
  representation()->destroyGraphicalBullet(bullet->graphicalBullet());
  delete bullet;
}

void model::GameState::playerL_elevate(input::Elevation elevation) {
  switch (elevation) {
    case input::Elevation::LOWER:
      rotateTank1_gun = -GUN_STEPRADIAN;
      break;

    case input::Elevation::RAISE:
      rotateTank1_gun = GUN_STEPRADIAN;
      break;

    case input::Elevation::STOP:
      rotateTank1_gun = 0;
  }
}

void model::GameState::playerL_move(input::Movement movement) {
  switch (movement) {
    case input::Movement::FORWARD:
      moveTank1 = QVector2D(3, 0);
      break;

    case input::Movement::BACKWARD:
      moveTank1 = QVector2D(-3, 0);
      break;

    case input::Movement::STILL:
      moveTank1 = QVector2D(0, 0);
  }
}

void model::GameState::playerL_shoot() {
  QLineF line;
  line.setP1(tank1->barrel()->position() - tank1->barrel()->rotateCenter());
  line.setAngle(-tank1->barrel()->angle() * (180 / M_PI)); //from radians to degrees
  line.setLength(37.5); //is length of gun barrel

  createBullet(QVector2D(line.p2()), tank1->barrel()->angle(), 100, 20 + 100 * attentionP1);
}

void model::GameState::playerR_elevate(input::Elevation elevation) {
  switch (elevation) {
    case input::Elevation::LOWER:
      rotateTank2_gun = GUN_STEPRADIAN;
      break;

    case input::Elevation::RAISE:
      rotateTank2_gun = -GUN_STEPRADIAN;
      break;

    case input::Elevation::STOP:
      rotateTank2_gun = 0;
  }
}

void model::GameState::playerR_move(input::Movement movement) {
  switch (movement) {
    case input::Movement::FORWARD:
      moveTank2 = QVector2D(-3, 0);
      break;

    case input::Movement::BACKWARD:
      moveTank2 = QVector2D(3, 0);
      break;

    case input::Movement::STILL:
      moveTank2 = QVector2D(0, 0);
  }
}

void model::GameState::playerR_shoot() {
  QLineF line;
  line.setP1(tank2->barrel()->position() - tank2->barrel()->rotateCenter());
  line.setAngle(-tank2->barrel()->angle() * (180 / M_PI)); //from radians to degrees
  line.setLength(-37.5); //length of gun barrel

  createBullet(QVector2D(line.p2()), tank2->barrel()->angle(), -100, 20 + 100 * attentionP2);
}

model::Tank * model::GameState::getTank1() const {
  return tank1;
}

model::Tank * model::GameState::getTank2() const {
  return tank2;
}

void model::GameState::attentionP1Changed(unsigned char attention) {
  attentionP1 = (float) attention / 100;
}

void model::GameState::attentionP2Changed(unsigned char attention) {
  attentionP2 = (float) attention / 100;
}

float model::GameState::getAttentionP1() const {
  return attentionP1;
}

float model::GameState::getAttentionP2() const {
  return attentionP2;
}

void model::GameState::connectThinkGears(input::ThinkGear * player1, input::ThinkGear * player2) {
  if (player1 != NULL) {
    QObject::connect(player1, SIGNAL(attention(unsigned char)), this, SLOT(attentionP1Changed(unsigned char)));
  }

  if (player2 != NULL) {
    QObject::connect(player2, SIGNAL(attention(unsigned char)), this, SLOT(attentionP2Changed(unsigned char)));
  }
}

