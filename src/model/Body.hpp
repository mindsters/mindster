#ifndef _BODY_HPP
#define _BODY_HPP

#include <QPointF>
#include <QPolygonF>
#include <QVector2D>

namespace graphics {
class GameObject;
}

namespace model {

/*!
 * Class for model objects with a body.
 */
class Body : public QObject {
    Q_OBJECT
  private:
    //! Position of the body.
    QPointF m_position;
    //! Point around which is done the rotation of Body
    const QPointF m_rotateCenter;
    //! Angle of rotation of the body.
    qreal m_angle;
  protected:
    //! Shape of the body.
    QPolygonF shape;
  public:
    Body(const QPointF & position, qreal angle, const QPolygonF & shape, graphics::GameObject * gameObject, const QPointF & rotateCenter = QPointF(0, 0));
    //! Translates the body.
    virtual void move(const QVector2D & shift);
    //! Rotates the body.
    virtual void rotate(qreal turn);
    //! Places the shape into its position.
    QPolygonF emplace() const;
    //! Returns the position.
    QPointF position() const;
    //! Returns the angle of rotation.
    qreal angle() const;
    //! Returns the axis point of the rotation.
    QPointF rotateCenter() const;
};

}

#endif
