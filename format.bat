@echo off
setlocal

set ARTISTIC_STYLE_OPTIONS=.astylerc

astyle --suffix=none --recursive src/*.cpp src/*.hpp

endlocal