#!/bin/bash

ARTISTIC_STYLE_OPTIONS=.astylerc astyle --suffix=none --recursive "src/*.cpp" "src/*.hpp"
