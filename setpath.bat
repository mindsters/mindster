@echo off
setlocal enabledelayedexpansion

set PATH=
if not "%~1" == "" (
  for /f "Tokens=* Delims=" %%x in (%1) do (
    if [!PATH!] == [] (
      set PATH=!PATH!%%x
    ) else (
      set PATH=!PATH!;%%x
    )
  )
)

REM echo "%PATH%"
shift

set CMD=
:concat
if "%~1" == "" goto out
  if [!CMD!] == [] (
    set CMD=!CMD!%1
  ) else (
    set CMD=!CMD! %1
  )
shift
goto concat
:out

%CMD%

endlocal